<?php get_header(); ?>


<?php 
$category = $wp_query->get_queried_object();
$category_slug = $category->slug;
$category_name = $category->name;
?>
<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs"><span><span><a href="https://balfin.al/"><?php _e("Home" , "balfin")  ?></a> &gt; <a href="https://balfin.al/news"><?php _e("News" , "balfin")  ?></a> &gt; <span class="breadcrumb_last" aria-current="page"><?php echo $category_name; ?></span></span></span></p>
				</div>
			</div>
		</div>
	</div>	
</section>


<div class="section-all-posts">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-10">
				<div class="posts-wrapper">
					<?php 
					 $args = array(
					 	'post_type' => 'post',
				        'posts_per_page' => 10,
				        'tag' => $category_slug
				        );
				    $loop = new WP_Query( $args );
				     ?>
					<?php echo do_shortcode('[ajax_load_more id="2741430537" post_type="post" posts_per_page="10" tag="' . $category_slug . '"  scroll="false"]'); ?>
				</div>
			</div>
			<div class="cell medium-2">
				<div class="filter-holder">
					<?php echo do_shortcode( '[searchandfilter headings="Tags" types="radio" fields="post_tag" hide_empty="1"]' ); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>