<?php get_header(); ?>

<div class="search-results">
     <?php if ( have_posts() ) : ?>


           <div class="grid-container">
                <div class="grid-x">
                    <div class="cell">
                        <div class="default-title small"><?php printf( __( 'Search Results for: %s', 'shape' ), '<b>' . get_search_query() . '</b>' ); ?></div>
                        <div class="section-all-posts">
                            <div class="grid-container">
                                <div class="grid-x">
                                    <div class="cell medium-10">
                                        <div class="posts-wrapper">
                                            <?php while ( have_posts() ) : the_post(); ?>
                                                <div class="single-post-wrapper">
                                                    <div class="grid-x">
                                                        <div class="cell medium-4">
                                                            <a class="img-holder" href="<?php the_permalink(); ?>">
                                                                <?php the_post_thumbnail(); ?>
                                                            </a>
                                                        </div>
                                                        <div class="cell medium-8">
                                                            <div class="post-content">
                                                                <div class="date"><?php $post_date = get_the_date( 'F j, Y' ); echo $post_date; ?></div>
                                                                <div class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                                                <div class="content-holder"><?php echo substr(get_the_excerpt(), 0,200)."…"; ?></div>
                                                                <div class="tags-holder">
                                                                    <?php the_tags( '', ' , ', '' ); ?>
                                                                </div>
                                                                <a href="<?php the_permalink(); ?>" class="read-more"><?php _e("Read More" , "balfin")  ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>

    <?php else : ?>
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell">
                    <div class="default-title small"><?php printf( __( 'No Results found for: %s', 'shape' ), '<b>' . get_search_query() . '</b>' ); ?></div>
                </div>
            </div>
        </div>

    <?php endif; ?>
</div>

<?php get_footer(); ?>

