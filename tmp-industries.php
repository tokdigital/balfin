<?php /* Template Name: Industry */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>
			</div>
		</div>
	</div>	
</section>

<section class="section-single-industry-first">
	<div class="grid-container full">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				<div class="text-holder">
					<div class="text-box">
						<div><h1 class="default-title"><?php the_title(); ?></h1></div>
						<!-- <div class="divider-vertical"></div> -->
						<?php the_content(); ?>
						<!-- <a href="#" class="read-more">Read More</a> -->
					</div>
				</div>
			</div>
			<div class="cell medium-6">
				<?php the_post_thumbnail(); ?>
			</div>
		</div>
	</div>
</section>


<?php 
	if( have_rows('industry_rows') ):
	$counter = 1;

    while( have_rows('industry_rows') ) : the_row(); 
		$color = 'white';
    	if($counter % 2 !== 0) {
			$color = 'gray';
    	}
    	$newtitle = sanitize_title(get_sub_field('title'));
	?>
<?php if($counter % 2 ==0) {  ?>

<div class="section-single-industry-content <?php echo($color); ?>" id="<?php echo($newtitle); ?>">
	<div class="single-wrapper">
		<div class="grid-x">
			<div class="cell medium-1">
				<div class="logo-holder">
					
				</div>
			</div> 
			
			<div class="cell medium-5">
				<div class="text-holder">
					<div class="text-box">
						<?php if (get_sub_field('title')) {  ?>
						<div class="default-title"><?php the_sub_field('title'); ?></div>
						<?php } ?>
						<?php if (get_sub_field('sub_title')) {  ?> 
						<div class="sub-title small"><?php the_sub_field('sub_title'); ?></div>
						<?php } ?>
						<div class="logo-wrapper">
							<!-- <div class="divider-vertical"></div> -->
							<?php if (get_sub_field('logo')) {  ?>
								<?php if (get_sub_field('link')) {  ?>
								<a href="<?php echo get_sub_field('link'); ?>" target="_blank" class="logo-link">
									<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
								</a>
								<?php } else { ?>
									<div class="logo-link">
										<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
									</div>
								<?php } ?>
							<?php } ?> 
						</div>
						<?php the_sub_field('content'); ?>
						<?php if (get_sub_field('link')) {  ?>
							<a href="<?php echo get_sub_field('link'); ?>" target="_blank" class="read-more"><?php _e("Visit Website" , "balfin")  ?></a>
						<?php } ?>
					</div>
				</div>
			</div> 

			<div class="cell medium-6">
				<div class="photo-holder">
					<?php 
					$images = get_sub_field('photos');
					if( $images ):  $newcounter = 0; ?>
						<div class="single-photo photo-left">
			            <?php foreach( $images as $image ): ?> 
							<?php if ($newcounter < 1) { ?>
								<div>
									<img src="<?php echo esc_url($image['url']); ?>" alt="">
								</div>
							<?php } ?>
			            <?php $newcounter ++;endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>	
			
		</div>
	</div>
</div>

<?php } else { ?>
<div class="section-single-industry-content <?php echo($color); ?>" id="<?php echo($newtitle); ?>">
	<div class="single-wrapper">
		<div class="grid-x">
			<div class="cell medium-6">
				<div class="photo-holder">
					<?php 
					$images = get_sub_field('photos');
					if( $images ):  $newcounter = 0; ?>
						<div class="single-photo photo-left">
			            <?php foreach( $images as $image ): ?> 
							<?php if ($newcounter < 1) { ?>
								<div>
									<img src="<?php echo esc_url($image['url']); ?>" alt="">
								</div>
							<?php } ?>
			            <?php $newcounter ++;endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>	

			<div class="cell medium-5">
				<div class="text-holder">
					<div class="text-box">
						<?php if (get_sub_field('title')) {  ?>
						<div class="default-title"><?php the_sub_field('title'); ?></div>
						<?php } ?>
						<?php if (get_sub_field('title')) {  ?> 
						<div class="sub-title small"><?php the_sub_field('sub_title'); ?></div>
						<?php } ?>
						<div class="logo-wrapper">
							<!-- <div class="divider-vertical"></div> -->
							<?php if (get_sub_field('logo')) {  ?>
								<?php if (get_sub_field('link')) {  ?>
								<a href="<?php echo get_sub_field('link'); ?>" target="_blank" class="logo-link">
									<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
								</a>
								<?php } else { ?>
									<div class="logo-link">
										<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
									</div>
								<?php } ?>
							<?php } ?> 
						</div>
						<?php the_sub_field('content'); ?>
						<?php if (get_sub_field('link')) {  ?>
							<a href="<?php echo get_sub_field('link'); ?>" target="_blank" class="read-more"><?php _e("Visit Website" , "balfin")  ?></a>
						<?php } ?>
					</div>
				</div>
			</div>	
			
			<div class="cell medium-1">
				<div class="logo-holder">
					
				</div>
			</div>
		</div>
	</div>
</div>

<?php } ?>

<?php $counter++;endwhile;endif; ?> 

	

<?php endwhile;endif; ?>
<?php get_footer(); ?>