<?php /* Template Name: History */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs">
					<span><span><a href="https://balfin.al/"><?php _e("Home" , "balfin")  ?></a> &gt; <span><span><?php _e("About Us" , "balfin")  ?></span> &gt; <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></span>
					</p>
				</div>
			</div>
		</div>
	</div>	
</section>

<div class="grid-container">
	<div class="grid-x">
		<div class="cell medium-12">
			<h1 class="big-title">
				<?php the_title(); ?>
			</h1>
		</div>
	</div>
</div>


<!-- <div class="section-history">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="history-slider">
					<?php 
						if (have_rows('history_rows')): while(have_rows('history_rows')): the_row();
					 ?>
					<div>
						<div class="grid-x">
							<div class="cell medium-6">
								<div class="image-wrapper">
									<?php if (get_field('photo_1')) { ?>
										<img src="<?php the_sub_field('photo_1') ?>" alt="">
										<img src="<?php the_sub_field('photo_2') ?>" alt="">
									<?php } else { ?>
										<img src="<?php bloginfo('template_url') ?>/img/news.jpg" alt="">
										<img src="<?php bloginfo('template_url') ?>/img/news.jpg" alt="">
									<?php } ?>
								</div>
							</div>
							<div class="cell medium-6">
								<div class="content">
									<div class="gotonext">
										<img src="<?php bloginfo('template_url') ?>/img/arrow-right-red.svg" alt="">
									</div>
									<div class="top-row">
										<?php if (get_sub_field('logo') ){ ?>
										<div class="logo"><img src="<?php the_sub_field('logo') ?>" alt=""></div>
										<?php } else { ?>
										<div class="logo"><img src="<?php bloginfo('template_url') ?>/img/logo.svg" alt=""></div>
										<?php } ?>
										<div class="year"><?php the_sub_field('year'); ?></div>
									</div>
									<div class="default-title"><?php the_sub_field('title'); ?></div>
									<?php the_sub_field('content'); ?>
								</div>
							</div>
						</div>
					</div> 
					<?php endwhile;endif; ?>
				</div>
				<div class="history-dots">
					<?php 
						if (have_rows('history_rows')): while(have_rows('history_rows')): the_row();
					 ?>
					<div>
						<div class="single-dot">
							<div class="year"><?php the_sub_field('year'); ?></div>
							<div class="dot"></div>
						</div>
					</div>
					<?php endwhile;endif; ?>
				</div>
			</div>
		</div>
	</div>
</div> -->


<div class="section-history" style="background: #fff7f7;">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="history-dots">
					<?php 
						if (have_rows('history_rows')): while(have_rows('history_rows')): the_row();
					 ?>
					<div>
						<div class="logos-holder">
							<?php 
								if (have_rows('companies')): while(have_rows('companies')): the_row();
							 ?>
								<?php if (get_sub_field('logo') ){ ?>
								<div class="logo"><img src="<?php the_sub_field('logo') ?>" alt=""></div>
								<?php } else { ?>
								<!-- <div class="logo"><img src="<?php bloginfo('template_url') ?>/img/logo.svg" alt=""></div> -->
								<?php } ?>
							<?php endwhile;endif; ?> 
						</div>	
						<div class="single-dot">
							<div class="year"><?php the_sub_field('year'); ?></div>
							<div class="dot"></div>
						</div>
						<div class="logos-holder leave">
							<?php 
								if (have_rows('companies')): while(have_rows('companies')): the_row();
							 ?>
								<?php if (get_sub_field('logo2') ){ ?>
								<div class="logo"><img src="<?php the_sub_field('logo2') ?>" alt=""></div>
								<?php } else { ?>
								<!-- <div class="logo"><img src="<?php bloginfo('template_url') ?>/img/logo.svg" alt=""></div> -->
								<?php } ?>
							<?php endwhile;endif; ?> 
						</div>
					</div>
					<?php endwhile;endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="section-history">
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-10">
				<div class="history-info">
					<!-- <?php 
						if (have_rows('history_rows')): while(have_rows('history_rows')): the_row();
					 ?>
					<div>
						<div class="logos-holder">
							<ul>
							<?php 
								if (have_rows('companies')): while(have_rows('companies')): the_row();
							 ?> 
								<li><?php the_sub_field('content'); ?></li>
							<?php endwhile;endif; ?> 
							 </ul>
						</div>	 
					</div>
					<?php endwhile;endif; ?> -->

					<div>
						<div class="history-content">

							<?php if( have_rows('years') ): ?>
   								<?php while( have_rows('years') ): the_row();  ?>

									<?php the_sub_field('content'); ?>

 				 				<?php endwhile; ?>
							<?php endif; ?>


						</div>	 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- <div class="section-new-history">
	<div class="grid-container">
		<?php 
			if (have_rows('history_rows')): while(have_rows('history_rows')): the_row();
		 ?>
		<div class="grid-x">
			<div class="cell medium-2 small-2">
				<div class="year"><?php the_sub_field('year'); ?></div>
			</div>
			<div class="cell medium-1 small-2">
				<div class="circle-holder">
					<div class="circle"></div>
				</div>
			</div>
			<div class="cell medium-9 small-8">
				<div class="content">
					<?php 
						if (have_rows('companies')): while(have_rows('companies')): the_row();
					 ?>
						<div class="single-company">
							<div class="top-row">
								<?php if (get_sub_field('logo') ){ ?>
								<div class="logo"><img src="<?php the_sub_field('logo') ?>" alt=""></div>
								<?php } else { ?>
								<div class="logo"><img src="<?php bloginfo('template_url') ?>/img/logo.svg" alt=""></div>
								<?php } ?>
								<div class="title">
									<?php the_sub_field('title'); ?>
								</div>
							</div>
							<div class="description">
								<?php the_sub_field('content'); ?>
							</div>
						</div>
					<?php endwhile;endif; ?> 
				</div>
			</div>
		</div>
		<?php endwhile;endif; ?>
	</div>
</div> -->

	

<?php endwhile;endif; ?>
<?php get_footer(); ?>