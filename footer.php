 <div class="footer" id="footer">
 	<div class="grid-container">
 		<div class="grid-x grid-padding-x">
 			<div class="cell medium-4">
 				<div class="footer-logo">
 					<img src="<?php bloginfo('template_url') ?>/img/Balfin 30 Logo white-02.png" alt="">
 				</div>
 				<p class="footer-text">
 					<!-- BALFIN Group is the most significant and successful investment groups in the Western Balkans region, with a net unconsolidated turnover of more than 620 million EUR for 2019. -->
 					<?php _e("BALFIN Group is the most significant and successful investment group in the Western Balkans region.", "balfin")  ?>
 				</p>
 				<div class="footer-title" style="margin-bottom: 0;"><?php _e("Follow Us", "balfin")  ?></div>
 				<div class="socials">
 					<a target="_blank" href="https://www.linkedin.com/company/balfingroup"><img src="<?php bloginfo('template_url') ?>/img/in.svg" alt=""></a>
 					<a target="_blank" href="https://www.facebook.com/Balfin.Group"><img src="<?php bloginfo('template_url') ?>/img/fb.svg" alt=""></a>
 					<a target="_blank" href="https://www.instagram.com/balfingroup/"><img src="<?php bloginfo('template_url') ?>/img/insta.svg" alt=""></a>
 					<a target="_blank" href="https://www.youtube.com/channel/UCfsqwU18xRnFUz6b2RE2bOQ"><img src="<?php bloginfo('template_url') ?>/img/yt-white.svg" alt=""></a>
 				</div>
 			</div>
 			<div class="cell medium-4">
 				<div class="footer-title"><?php _e("Quick Links", "balfin")  ?></div>
 				<div class="grid-x">
 					<div class="cell medium-4">
 						<div class="footer-menu">
 							<a href="<?php echo site_url(); ?>"><?php _e("Home", "balfin")  ?></a>
 							<a href="<?php echo site_url(); ?>/about-us/balfin-group"><?php _e("About Us", "balfin")  ?></a>
 							<a href="<?php echo site_url(); ?>/partners"><?php _e("Partners", "balfin")  ?></a>
 							<a href="<?php echo site_url(); ?>/industries"><?php _e("Industries", "balfin")  ?></a>
 							<a href="<?php echo site_url(); ?>/human-resources/career-opportunities/"><?php _e("Careers", "balfin")  ?></a>
 						</div>
 					</div>
 					<div class="cell medium-1"></div>
 					<div class="cell medium-7">
 						<div class="footer-menu">
 							<a href="<?php echo site_url(); ?>/human-resources"><?php _e("Human Resources", "balfin")  ?></a>
 							<a href="<?php echo site_url(); ?>/sustainability/"><?php _e("Sustainable", "balfin")  ?></a>
 							<a href="<?php echo site_url(); ?>/news"><?php _e("News", "balfin")  ?></a>
 							<a href="<?php echo site_url(); ?>/contact"><?php _e("Contact", "balfin")  ?></a>

 						</div>
 					</div>
 				</div>
 			</div>
 			<div class="cell medium-4">
 				<div class="footer-title"><?php _e("Subscribe", "balfin")  ?></div>
 				<p class="footer-text">
 					<?php _e("Join our mailing list to receive news and announcements. Enter your e-mail address and get notifications.", "balfin")  ?>
 				</p>
 				<div class="subscribe">
 					<div class="form-holder">
 						<!-- <form action="">
							<input type="text" name="" placeholder="E-mail address">
							<input type="submit" name="" value="Subscribe">
						</form> -->
 						<?php echo do_shortcode('[ninja_form id=2]	'); ?>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>
 	<div class="legal-links">
 		<!-- <a href="<?php echo site_url(); ?>/terms-and-conditions">Terms and Conditions</a>
		<a href="<?php echo site_url(); ?>/cookie-policy">Cookie Policy</a>
		<a href="<?php echo site_url(); ?>/website-legal-disclaimer">Website Legal Disclaimer</a> -->
 		<a href="<?php echo site_url(); ?>/privacy-policy"><?php _e("Privacy Policy", "balfin")  ?></a>
 		<div class="web-links" style="
		    color: #fff;
		    justify-self: flex-end;
		    margin-left: auto;
		">
 			<a href="https://tok.al" target="_blank" style="
		    margin: 2px;
		">Website design &amp; Development</a> by <a href="http://tok.al" style="
		    margin: 2px;
		">Tok Digital Agency</a>
 		</div>
 	</div>
 </div>


 <?php wp_footer(); ?>
 <?php
	global $post;
	$id = $post->post_parent;
	?>
 <?php if ($id == 22) { ?>
 	<script>
 		$('[data-menu="2"]').click();
 	</script>
 <?php } ?>

 <?php if ($id == 7) { ?>
 	<script>
 		$('[data-menu="1"]').click();
 	</script>
 <?php } ?>

 <?php if ($id == 2432) { ?>
 	<script>
 		$('[data-menu="6"]').click();
 	</script>
 <?php } ?>















 </body>

 </html>