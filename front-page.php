<?php get_header(); ?>

<!-- <section class="section-hero  wow fadeIn" data-wow-delay="0.4s" data-wow-duration="1s">
	<div class="grid-container full">
		<div class="hero-frame">
			<div 
				id="gl" 
				data-imageOriginal="<?php bloginfo('template_url') ?>/img/hero1.jpg" 
				data-imageDepth="<?php bloginfo('template_url') ?>/img/herobg.jpg" 
				data-horizontalThreshold="35" 
				data-verticalThreshold="15">
			</div> 
			<div class="hero-slider">
				<div>
					<div class="single-slide">
						<div class="gotonext-hero">
							<img src="<?php bloginfo('template_url') ?>/img/arrow-right-white.svg" alt="">
						</div>
						<div class="text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus, sunt!</div>
					</div>
				</div>
				<div>
					<div class="single-slide">
						<div class="gotonext-hero">
							<img src="<?php bloginfo('template_url') ?>/img/arrow-right-white.svg" alt="">
						</div>
						<div class="text">Delectus at harum porro maxime? Facilis, maiores perspiciatis, iure iste blanditiis!</div>
					</div>
				</div>
				<div>
					<div class="single-slide">
						<div class="gotonext-hero">
							<img src="<?php bloginfo('template_url') ?>/img/arrow-right-white.svg" alt="">
						</div>
						<div class="text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus, sunt!</div>
					</div>
				</div>
				<div>
					<div class="single-slide">
						<div class="gotonext-hero">
							<img src="<?php bloginfo('template_url') ?>/img/arrow-right-white.svg" alt="">
						</div>
						<div class="text">Delectus at harum porro maxime? Facilis, maiores perspiciatis, iure iste blanditiis!</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->

<section class="section new-hero">
	<!-- <div>
		<section class="section-about">
			<div class="grid-container full">
				<div class="grid-x">
					<div class="cell medium-8">
						<img src="https://balfin.al/wp-content/uploads/2021/08/We-ARE-BALFIN.jpg" alt="" class="wow fadeInRight" style="z-index: -1;">
						 <div class="video-holder">
						 	<video src="<?php bloginfo('template_url') ?>/img/test.mp4" controls></video>
						 </div>
					</div>
					<div class="cell medium-4">
						<div class="text-holder wow fadeInRight">
							<div class="text-box">
								<div class="default-title has-decor red">We are BALFIN
									<div class="gotonext-hero">
										<img src="<?php bloginfo('template_url') ?>/img/arrow-right-red-thin.svg" alt="">
									</div>
								</div>
								<p>Among the most important investment groups in Western Balkans, with around 6000 employees, in seven countries.</p>
								<a href="https://balfin.al/about-us/balfin-group/" class="read-more">Read More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div> -->
	<!-- <div>
		<section class="section-about">
			<div class="grid-container full">
				<div class="grid-x">
					<div class="cell medium-8">
						 <iframe width="560" height="315" src="https://www.youtube.com/embed/nrXUBhYXJyM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
					<div class="cell medium-4">
						<div class="text-holder wow fadeInRight">
							<div class="text-box">
								<div class="default-title has-decor red">We are BALFIN 
								</div>
								<p>One of the most significant and successful investment groups in the Western Balkans region, operating in 7 countries and employing more than 6000 people.</p>
								<a href="https://balfin.al/about-us/balfin-group/" class="read-more">Read More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<img src="<?php the_sub_field('slider_image') ?>" alt="" class="wow fadeInRight" style="z-index: -1;">
	</div> -->
	<?php if (have_rows('hero_slider')) : while (have_rows('hero_slider')) : the_row(); ?>
			<div>
				<section class="section-about">
					<div class="grid-container full">
						<div class="grid-x">
							<div class="cell medium-8">
								<?php if (!empty(get_sub_field('slider_video'))) {
								?>
									<video loop autoplay muted class="wow fadeInRight" style="z-index: -1;">
										<source src="<?php the_sub_field('slider_video'); ?>" type="video/mp4">
									</video>
								<?php
								} else { ?>
									<img src="<?php the_sub_field('slider_image') ?>" alt="" class="wow fadeInRight" style="z-index: -1;">
								<?php } ?>
							</div>
							<div class="cell medium-4">
								<div class="text-holder wow fadeInRight">
									<div class="text-box">
										<div class="default-title has-decor red"><?php the_sub_field('title') ?>
											<!-- <div class="gotonext-hero">
										<img src="<?php bloginfo('template_url') ?>/img/arrow-right-red-thin.svg" alt="">
									</div> -->
										</div>
										<?php the_sub_field('content') ?>
										<a href="<?php the_sub_field('link') ?>" class="read-more"><?php _e("Read More", "balfin")  ?></a>
									</div>
								</div>
							</div>

						</div>
					</div>
				</section>
			</div>
	<?php endwhile;
	endif; ?>

</section>

<section class="section-group">
	<div class="grid-container ">
		<div class="grid-x">
			<div class="cell medium-5">
				<div class="text-holder">
					<div class="text-box wow fadeInLeft">
						<div class="grid-x">
							<div class="cell medium-12">
								<h1 class="default-title">
									<?php _e("Our Companies", "balfin")  ?>
									<!-- <div class="arrow-holder">
										
									</div> -->
								</h1>
								<p><?php _e("One of our goals and advantages is to have a diversified investments portfolio, which enables us to contribute in many areas of the economy.", "balfin")  ?></p>
								<a class="read-more" href="<?php echo site_url(); ?>/industries/"><?php _e("Read More", "balfin")  ?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="cell medium-7">
				<div class="group-blocks">
					<!-- <div class="gotoprev-industries"><img src="<?php bloginfo('template_url') ?>/img/arrow-left-red-thin.svg" alt=""></div> -->
					<div class="gotoprev-industries"><img src="<?php bloginfo('template_url') ?>/img/angle-left.svg" alt=""></div>
					<!-- <div class="gotonext-industries"><img src="<?php bloginfo('template_url') ?>/img/arrow-right-red-thin.svg" alt=""></div> -->
					<div class="gotonext-industries"><img src="<?php bloginfo('template_url') ?>/img/angle-right.svg" alt=""></div>
					<div class="grid-x">
						<div class="cell medium-12">
							<div class="industries-slider">
								<?php
								$industries = get_field('industries');
								if ($industries) : $counter = 1;
									$delay = 0; ?>

									<?php foreach ($industries as $post) :
										setup_postdata($post); ?>
										<div>
											<div class="single-block wow fadeInRight <?php echo 'industry-' . $counter; ?>" <?php if ($delay < 0.8) { ?>data-wow-delay="<?php echo ($delay); ?>s" <?php } ?>>
												<div class="first-wrapper">
													<div class="gray-title">
														<!-- <span class="number"><?php echo $counter; ?></span> -->
														<div class="arrow-holder show-for-small-only">
															<div class="gotonext-industries"><img src="<?php bloginfo('template_url') ?>/img/arrow-right-red-thin.svg" alt=""></div>
														</div>
													</div>
													<div class="category"><?php the_title(); ?></div>
												</div>
												<div class="content-wrapper">
													<div class="featured-image">
														<a href="<?php the_permalink(); ?>" class="photo-link">
															<img src="<?php the_field('home_photo'); ?>" alt="">
														</a>
													</div>
													<div class="content">
														<div class="logo-row">
															<?php
															if (have_rows('industry_rows')) :
																while (have_rows('industry_rows')) : the_row(); ?>
																	<?php if (get_sub_field('logo')) {  ?>
																		<?php if (get_sub_field('link')) {  ?>
																			<a href="<?php echo get_sub_field('link'); ?>" target="_blank">
																				<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
																			</a>
																		<?php } else { ?>
																			<a href="#" target="_blank" onclick="return false;">
																				<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
																			</a>
																		<?php } ?>
																		<?php if ($counter == 7) { ?>
																			<a href="https://group.accor.com/en" target="_blank">
																				<img src="https://balfin.al/wp-content/uploads/2021/09/accor.svg" alt="" class="logo">
																			</a>
																		<?php } ?>
															<?php }
																endwhile;
															endif; ?>
														</div>
														<p><?php echo substr(get_the_excerpt(), 0, 220) . "…"; ?></p>
														<a href="<?php the_permalink(); ?>" class="read-more"><?php _e("Read More", "balfin")  ?></a>
													</div>
												</div>
											</div>
										</div>

									<?php $delay = $delay + 0.25;
										$counter++;
									endforeach; ?>
									<?php
									// Reset the global post object so that the rest of the page works correctly.
									wp_reset_postdata(); ?>
								<?php endif; ?>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<!-- <section class="section-stats">
	<div class="grid-container">
		<div class="grid-padding-x grid-x grid-margin-x">
			<div class="cell medium-4">
				<div class="single-stat">
					<div class="number">5000</div>
					<p><span>Over 5000 people</span> <br>work in our Group companies.</p>
				</div>
			</div>
			<div class="cell medium-4">
				<div class="single-stat">
					<div class="number">500</div>
					<p>Group Yearly Turnover is more than <br><span>500 million Euros.</span></p>
				</div>
			</div>
			<div class="cell medium-4">
				<div class="single-stat">
					<div class="number">1.2</div>
					<p>Group Total Assets are more than <br><span>1.2 billion USD.</span></p>
				</div>
			</div>
			<div class="cell medium-4">
				<a href="<?php echo site_url(); ?>/about-us/balfin-group/" class="single-stat-photo stat1 wow fadeInRight" data-wow-delay="0s">
					<img src="<?php bloginfo('template_url') ?>/img/stat1text.svg" class="text" alt="">
					<img src="<?php bloginfo('template_url') ?>/img/stat1image.png" class="image" alt="">
				</a>
			</div>
			<div class="cell medium-4">
				<a href="<?php echo site_url(); ?>/about-us/balfin-group/" class="single-stat-photo stat2 wow fadeInRight" data-wow-delay="0.4s">
					<img src="<?php bloginfo('template_url') ?>/img/stat2text.svg" class="text" alt="">
					<img src="<?php bloginfo('template_url') ?>/img/stat2image.png" class="image" alt="">
				</a>
			</div>
			<div class="cell medium-4">
				<a href="<?php echo site_url(); ?>/about-us/balfin-group/" class="single-stat-photo stat3 wow fadeInRight" data-wow-delay="0.8s">
					<img src="<?php bloginfo('template_url') ?>/img/stat3text.svg" class="text" alt="">
					<img src="<?php bloginfo('template_url') ?>/img/stat3image.png" class="image" alt="">
				</a>
			</div>
		</div>
	</div>
</section> -->


<!-- <div class="section-new-numbers">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-6">
				<div class="text-holder wow fadeInLeft">
					<div class="text-box">
						<div class="default-title f50">Annual Report</div>
						<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repudiandae, nemo ipsam mollitia?</p>
						<?php $cc = 0; ?>
						<?php if (have_rows('annual_reports', 2401)) : while (have_rows('annual_reports', 2401)) : the_row(); ?>
							<?php if ($cc == 0) { ?>
								<div class="single-file" style="width: 200px;max-width: 90%;margin: 0 auto; ">
									<?php if (get_sub_field('cover')) { ?>
										<a target="_blank" href="<?php the_sub_field('file') ?>" class="photo">
											<img src="<?php the_sub_field('cover') ?>" alt="">
										</a>
									<?php } else { ?>
										<a target="_blank" href="<?php the_sub_field('file') ?>" class="icon">
											<img src="<?php bloginfo('template_url') ?>/img/pdf.svg" alt="">
										</a>
									<?php } ?>
									<a target="_blank" href="<?php the_sub_field('file') ?>" class="title">
										<?php the_sub_field('title') ?>
									</a>
								</div>
						<?php }
								$cc++;
							endwhile;
						endif; ?>
					</div>
				</div>
			</div>
			<div class="cell medium-6">
				<div class="new-numbers-wrapper fadeInRight wow">
					<div class="default-title f50">Balfin in Numbers</div>
					<div class="new-numbers">
						<div class="single-number-accordion">
							<div class="title open" data-acc-open="1">
								Assets
							</div>
							<div class="content open" data-acc-content="1">
								<div class="number">1.2+</div>
								<div class="text">Billion USD<br> Assets</div>
							</div>
						</div>
						<div class="single-number-accordion">
							<div class="title" data-acc-open="2">
								Employees
							</div>
							<div class="content" data-acc-content="2">
								<div class="number">6000+</div>
								<div class="text">People working<br>With us</div>
							</div>
						</div>
						<div class="single-number-accordion">
							<div class="title" data-acc-open="3">
								Countries
							</div>
							<div class="content" data-acc-content="3">
								<div class="number">7</div>
								<div class="text">European <br>Countries</div>
							</div>
						</div>
						<div class="single-number-accordion">
							<div class="title" data-acc-open="4">
								Turnover
							</div>
							<div class="content" data-acc-content="4">
								<div class="number">500+</div>
								<div class="text">Million euros<br>Yearly turnover</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div> -->

<section class="section-about careers sus">
	<div class="grid-container full">
		<div class="grid-x">
			<div class="cell medium-8">
				<img src="/wp-content/themes/balfin/img/balfin-group-sustainibility-social.jpg" alt="" class="wow fadeInRight" style="z-index: -1;">
			</div>
			<div class="cell medium-4">
				<div class="text-holder wow fadeInRight">
					<div class="text-box">
						<div class="default-title">
							<?php _e("Sustainable Development", "balfin")  ?>
						</div>
						<p><?php _e("We are always engaged in serious efforts for social welfare, because of moral obligation and sustainability. Our CSR strategy ties in with overall Group business strategy and addresses real social needs.", "balfin")  ?></p>
						<a href="<?php echo site_url() ?>/sustainability" class="read-more"><?php _e("Read More", "balfin")  ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<!-- <div class="section-balfin-numbers">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="default-title white center">BALFIN in Numbers</div>
			</div>
		</div>
		<div class="numbers">
			<div class="grid-x">
				<div class="cell medium-3 small-6">
					<div class="single-number wow fadeInRight" data-wow-delay="0s">
						<div class="number">1.2+</div>
						<div class="text">Billion USD<br> Assets</div>
					</div>
				</div>
				<div class="cell medium-3 small-6">
					<div class="single-number wow fadeInRight" data-wow-delay="0.3s">
						<div class="number">6000+</div>
						<div class="text">People working<br>With us</div>
					</div>
				</div>
				<div class="cell medium-3 small-6">
					<div class="single-number wow fadeInRight" data-wow-delay="0.6s">
						<div class="number">7</div>
						<div class="text">European <br>Countries</div>
					</div>
				</div>
				<div class="cell medium-3 small-6">
					<div class="single-number wow fadeInRight" data-wow-delay="0.9s">
						<div class="number">500+</div>
						<div class="text">Million euros<br>Yearly turnover</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->


<!-- <section class="section why-us" id="why-us">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-5">
				<div class="default-title has-decor yellow">
					BALFIN <br>in Numbers
				</div>
				<p>Balfin Group, the biggest investment group in Albania and the strongest in Balkan is distinguished for large investments in construction field. This group started the activity in the field of construction in the early of 2000, firstly by focusing…</p>
			</div>
			<div class="cell medium-7">
				<div class="circles-holder">
					<div class="single-circle-holder wow fadeInUp" data-wow-delay="0">
						<div class="circle" id="circles-1">
						</div>
						<div class="new-text">Billion USD<br> Assets</div>
					</div>
					<div class="single-circle-holder wow fadeInUp" data-wow-delay="0.5">
						<div class="circle" id="circles-2">
						</div>
						<div class="new-text">People working<br>With us</div>
					</div>
					<div class="single-circle-holder wow fadeInUp" data-wow-delay="1">
						<div class="circle" id="circles-3">
						</div>
						<div class="new-text">Industries</div>
					</div>
					<div class="single-circle-holder wow fadeInUp" data-wow-delay="1.5">
						<div class="circle" id="circles-4">
						</div>
						<div class="new-text">Million euros<br>Yearly turnover</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->


<section class="section-news">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="default-title center"><?php _e("Latest Stories", "balfin")  ?></div>
				<!-- <p class="section-desc">Balfin Group, the biggest investment group in Albania and the strongest in Balkan is distinguished for large investments in construction field. This group started the activity in the field of construction in the early.</p> -->
			</div>
		</div>
		<div class="news-slider wow fadeInUp">
			<?php
			$args = array(
				'post_type' => 'post',
				'posts_per_page' => 10
			);
			$loop = new WP_Query($args);
			?>
			<?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
					<div>
						<div class="single-news">
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail(); ?>
							</a>
							<div class="content">
								<a href="<?php the_permalink(); ?>">
									<!-- <?php if (get_field('preview_title')) { ?>
							<span class="news-title"><?php the_field('preview_title'); ?></span>
							<?php } else { ?>
							<span class="news-title"><?php the_title(); ?></span>
							<?php } ?> -->
									<span class="news-title"><?php the_title(); ?></span>
								</a>
								<!-- <p><?php echo substr(get_the_excerpt(), 0, 120) . "…"; ?></p> -->
								<!-- <p><?php echo get_the_excerpt(); ?></p> -->
								<a href="<?php the_permalink(); ?>" class="read-more"><?php _e("Read More", "balfin")  ?></a>
							</div>
						</div>
					</div>

			<?php endwhile;
			endif;
			wp_reset_postdata(); ?>
		</div>
		<div class="button-holder">
			<a href="<?php echo site_url() ?>/media-center/news/" class="read-more center"><?php _e("See all Stories", "balfin")  ?></a>
		</div>
	</div>
</section>

<!-- <section class="section home-sus">
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-12">
				<div class="default-title  center">
					Sustainable Development
				</div>
			</div>
			<div class="cell medium-10">
				<p>We are always engaged in serious efforts for social welfare, because of moral obligation and sustainability. Our CSR strategy ties in with overall Group business strategy and addresses real social needs.</p>
			</div>
			<div class="cell medium-12">
				<img src="<?php bloginfo('template_url') ?>/img/home-sus2.jpg" alt="">
				<a href="<?php echo site_url() ?>/sustainable-development" class="read-more">Read More</a>
			</div>
		</div>
	</div>
</section> -->

<!-- <section class="section home-sus new-sus">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-6">
				<div class="new-sus-wrapper fadeInLeft wow">
					<div class="default-title f50">
						Sustainable Development
					</div>
					<p>We are always engaged in serious efforts for social welfare, because of moral obligation and sustainability. Our CSR strategy ties in with overall Group business strategy and addresses real social needs.</p>
					<img src="<?php bloginfo('template_url') ?>/img/home-sus2.jpg" alt="">
					<a href="<?php echo site_url() ?>/sustainable-development" class="read-more">Read More</a>
				</div>
			</div> 
			<div class="cell medium-6">
				<div class="covid-wrapper fadeInRight wow">
					<div class="default-title f50">
						Covid Approach
					</div>
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium recusandae vitae assumenda perferendis repellendus officia hic ipsum, sed. Voluptates recusandae laboriosam rerum, magni sit libero similique quis soluta culpa quibusdam suscipit odit praesentium minus non? Earum veniam, molestias perspiciatis! Eos soluta veritatis ipsam adipisci corrupti perspiciatis odit consequatur suscipit. Atque.</p>
				</div>
			</div> 
		</div>
	</div>
</section>
 -->

<!-- <section class="section-careers">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				<div class="image-wrapper wow fadeInLeft">
					<img src="<?php bloginfo('template_url') ?>/img/careers-banner.jpg" alt="">
				</div>
			</div>
			<div class="cell medium-1">
			</div>
			<div class="cell medium-5">
				<div class="content wow fadeInRight">
					<div class="default-title">Careers</div>
					<div class="divider-vertical"></div>
					<p>We are among the largest employers in the private sector in Albania. We are one of the biggest employers in the private sector in Albania. In Balfin Group companies work over 5000 individuals. The positioning our group has in the Albanian labor market makes it a very important player and an example to follow.</p>
					<a href="<?php echo site_url(); ?>/contact-us/" class="read-more">Contact Us</a>
				</div>
			</div>
		</div>
	</div>
</section> -->

<section class="section-about careers">
	<div class="grid-container full">
		<div class="grid-x">
			<div class="cell medium-8">
				<img src="<?php bloginfo('template_url') ?>/img/careers-banner.jpg" alt="" class="wow fadeInLeft" style="z-index: -1;">
			</div>
			<div class="cell medium-4">
				<div class="text-holder wow fadeInRight">
					<div class="text-box">
						<div class="default-title"><?php _e("Careers", "balfin")  ?></div>
						<p><?php _e("We provide careers for more than 5300 people, creating a working environment
characterized by security, equality, and employee commitment to the corporate values.", "balfin")  ?> <br><?php _e("In our progress strategy, people are the center of all our successes. This is why we are always seeking for professionals who have the energy, enthusiasm and necessary knowledge to join us.", "balfin")  ?></p>
						<a href="<?php echo site_url(); ?>/human-resources/career-opportunities/" class="read-more"><?php _e("Learn More", "balfin")  ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php get_footer(); ?>

<!-- <script src="<?php bloginfo('template_url') ?>/js/app2.js"></script> -->