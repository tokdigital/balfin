<?php get_header(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>
			</div>
		</div>
	</div>	
</section>

<div class="section-all-posts">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-10">
				<div class="posts-wrapper">
					<?php 
					 $args = array(
					 	'post_type' => 'post',
				        'posts_per_page' => 10,
				        'offset' => 1
				        );
				    $loop = new WP_Query( $args );
				     ?>
					<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
						<div class="single-post-wrapper">
							<div class="grid-x">
								<div class="cell medium-4">
									<a class="img-holder" href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail(); ?>
									</a>
								</div>
								<div class="cell medium-8">
									<div class="post-content">
										<div class="date"><?php $post_date = get_the_date( 'l F j, Y' ); echo $post_date; ?></div>
										<div class="post-title"><?php the_title(); ?></div>
										<div class="content-holder"><?php echo substr(get_the_excerpt(), 0,200)."…"; ?></div>
										<div class="tags-holder">
											<?php the_tags( '', ' , ', '' ); ?>
										</div>
										<a href="<?php the_permalink(); ?>" class="read-more"><?php _e("Read More" , "balfin")  ?></a>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile;endif;wp_reset_postdata(); ?>
				</div>
			</div>
			<div class="cell medium-2">
				<?php echo do_shortcode( '[searchandfilter headings="Categories,Tags" types="radio,radio" fields="category,post_tag"]' ); ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>