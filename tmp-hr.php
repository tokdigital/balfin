<?php /* Template Name: Human Resources */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>
			</div>
		</div>
	</div>	
</section>



<div class="grid-container">
	<div class="grid-x">
		<div class="cell medium-12">
			<div>
				<h1 class="big-title"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>

<div class="section-text no-pad-top">
	<div class="grid-container">
		<div class="grid-x">  
			<div class="cell medium-6">
				<div class="text-holder">
					<div class="text-block">
						<!-- <div class="default-title has-decor"><?php the_title(); ?></div> -->
						<?php the_content(); ?> 
					</div>
				</div>
			</div>
			<div class="cell medium-6">
				<div class="image-wrapper ">
					<img src="<?php bloginfo('template_url') ?>/img/hr-banner.jpg" alt="" style="max-height: 500px;">
				</div>
			</div>
		</div>
	</div>
</div>

<div class="section-three" id="internship-blocks">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<?php 
				$counter = 0;
				$colors = array('red','blue','yellow','green');
				if (have_rows('hr_blocks')) : while(have_rows('hr_blocks')) : the_row();
			 ?>
			<div class="cell medium-3">
				<div class="single-block  no-border <?php echo $colors[$counter] ?>">
					<?php if (get_sub_field('link')){ ?>
					<a class="img-holder" href="<?php the_sub_field('link') ?>"><img src="<?php the_sub_field('image') ?>" alt=""></a>
					<?php } else { ?>
					<div class="img-holder" data-hr="<?php echo $counter; ?>"><img src="<?php the_sub_field('image') ?>" alt=""></div>
					<?php } ?>
					<div class="title"><?php the_sub_field('title') ?></div>
					<?php if (get_sub_field('link')){ ?>
					<a href="<?php the_sub_field('link') ?>" class="read-more"><?php _e("Read More" , "balfin")  ?></a>
					<?php } else { ?>
					<div data-hr="<?php echo $counter; ?>" class="read-more"><?php _e("Read More" , "balfin")  ?></div>
					<?php } ?>
				</div>
			</div>
			<?php $counter++;endwhile;endif; ?>
		</div>
	</div>
</div>

<?php 
	$counter = 0;
	if (have_rows('hr_blocks')) : while(have_rows('hr_blocks')) : the_row();
 ?>  
<div class="member-popup hr-popup" data-popup="<?php echo $counter; ?>">
	<div class="popup-inner">
		<div class="close-popup">&times;</div>
		<div class="grid-x grid-padding-x"> 
			<div class="cell medium-12">
		<div class="title"><?php the_sub_field('title') ?></div>
		<?php the_sub_field('content'); ?>
		<?php if (have_rows('open_positions')) : ?>
		<div class="title"><?php _e("Open Positions" , "balfin")  ?></div>
		<div class="grid-x">
			<?php while(have_rows('open_positions')) : the_row(); ?>
			<div class="cell medium-3">
				<div class="single-position">
					<?php the_sub_field('position'); ?>
				</div>
			</div>
		<?php endwhile;endif; ?>
		</div>
		</div>
	</div>
	</div>
</div>  
<?php $counter++;endwhile;endif; ?>

<!-- <div class="section-hidden-contents">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell">
				<?php 
					$counter = 0;
					if (have_rows('hr_blocks')) : while(have_rows('hr_blocks')) : the_row();
				 ?>
				<div class="hidden-content" data2-content="<?php echo $counter; ?>" id="<?php echo $counter; ?>">
					<div class="default-title has-decor"><?php the_sub_field('title'); ?></div>
					<?php the_sub_field('content'); ?>
					<a class="read-more" data2-close="<?php echo $counter; ?>" href="#internship-blocks"><span class="readless">Show Less</span></a>
				</div>
				
				<?php $counter++;endwhile;endif; ?>
			</div>
		</div>
	</div>
</div> -->
	
<div class="section-vision b4">
	<div class="grid-container">  
		<div class="grid-x grid-padding-x">  
			<div class="cell medium-6">
				<div class="text-holder">
					<div class="text-block">

						<?php if( have_rows('internship') ): ?>
    						<?php while( have_rows('internship') ): the_row();  ?>

						<div class="default-title has-decor"><?php the_sub_field('title'); ?></div>
						<p><?php the_sub_field('content'); ?></p>
						<a target="_blank" href="https://b4students.com/internships/jobs" class="read-more"><?php _e("Visit Website" , "balfin")  ?></a>

	    					<?php endwhile; ?>
						<?php endif; ?>

					</div>
				</div>
			</div>
			<div class="cell medium-6">
				<div class="big-image fixed">
					<img src="<?php bloginfo('template_url') ?>/img/b4students.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 
<div class="section-internship">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-7">
				<div class="content">
					<div class="default-title">INTERNSHIP</div>
					<p>We provide full or part time professional practice for all qualified students in all our Group companies. The professional internship is a real contribution we provide for education and counseling of the future specialists and professionals, fulfilling one of our core missions on contributing to the community. </p>
					<div class="hidden-content" data-content="internship">
						<p>We offer the best professional environment for student to learn and gain experience during the time there are gaining university knowledge. <br>
During the internship phase, the students are acquainted closely with all business operations. This is an opportunity for the students to work on the projects of Balfin Group with some of the best professionals in the respective areas of the country.<br>
After each cycle of professional internship, we provides appropriate evaluations for each student being accompanied by recommendations that can serve for the future. We also use professional internships to recruit new employees for our companies. Many of our employees have joined our Group exactly from internships, showing dedication during completing tasks assigned at the time of professional internship.

</p>
					</div>
					<div class="read-more" data-open="internship"><span class="readmore">Show More</span><span class="readless">Show Less</span></div>
				</div>
			</div>
		</div>
	</div>
</div> -->


<?php endwhile;endif; ?>
<?php get_footer(); ?>