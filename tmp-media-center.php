<?php /* Template Name: Media Center */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs"><span><span><a href="https://balfin.al/"><?php _e("Home" , "balfin")  ?></a> &gt; <span><?php _e("Media Center" , "balfin")  ?> &gt; <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></span></p>
				</div>
			</div>
		</div>
	</div>	
</section>

<div class="section-main-news-new">
	<div class="grid-container">
		<?php 
		 $args = array(
		 	'post_type' => 'post',
	        'posts_per_page' => 1
	        );
	    $loop = new WP_Query( $args );
	     ?>
		<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
		<div class="grid-x">
			<div class="cell medium-6">
				<a class="img-holder" href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail(); ?>
				</a>
			</div>
			<div class="cell medium-6">
				<div class="post-content">
					<div class="date"><?php $post_date = get_the_date( 'F j, Y' ); echo $post_date; ?></div>
					<div><h1 class="post-title"><?php the_title(); ?></h1></div>
					<div class="content-holder"><?php echo substr(get_the_excerpt(), 0,300)."…"; ?></div>
					<div class="tags-holder">
						<?php the_tags( '', ' , ', '' ); ?>
					</div>
					<a href="<?php the_permalink(); ?>" class="read-more"><?php _e("Read More" , "balfin")  ?></a>
				</div>
			</div>
		</div>
		<?php endwhile;endif;wp_reset_postdata(); ?>
	</div>
</div>

<section class="section-news">
	<div class="grid-container"> 
		<div class="news-sliderr wow fadeInUp">
			<div class="grid-x grid-padding-x"> 
				<?php echo do_shortcode('[ajax_load_more id="2747430537" post_type="post" posts_per_page="9" scroll="false" offset="1"]'); ?>
			</div> 
		</div> 
	</div>
</section>

<!-- 
<div class="section-all-posts">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-10">
				<div class="posts-wrapper">
					<?php echo do_shortcode('[ajax_load_more id="2747430537" post_type="post" posts_per_page="10" scroll="false" offset="1"]'); ?>
				</div>
			</div>
			
		</div>
	</div>
</div> -->

<!-- <div class="single-post-wrapper">
	<div class="grid-x">
		<div class="cell medium-4">
			<a class="img-holder" href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail(); ?>
			</a>
		</div>
		<div class="cell medium-8">
			<div class="post-content">
				<div class="date"><?php $post_date = get_the_date( 'F j, Y' ); echo $post_date; ?></div>
				<div class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
				<div class="content-holder"><?php echo substr(get_the_excerpt(), 0,200)."…"; ?></div>
				<div class="tags-holder">
					<?php the_tags( '', ' , ', '' ); ?>
				</div>
				<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
			</div>
		</div>
	</div>
</div> -->

<?php endwhile;endif; ?>
<?php get_footer(); ?>