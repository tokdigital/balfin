<?php /* Template Name: Mission/Vision */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs">
					<span><span><a href="https://balfin.al/"><?php _e("Home" , "balfin")  ?></a> &gt; <span><span><?php _e("About Us" , "balfin")  ?></span> &gt; <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></span>
					</p>
				</div>
			</div>
		</div>
	</div>	
</section>

<div class="section-mission">
	<div class="grid-x">
		<div class="cell medium-5">
			<div class="big-image">
				<?php the_post_thumbnail(); ?>
			</div>
		</div>
		<div class="cell medium-1"></div>
		<div class="cell medium-6">
			<div class="text-block-holder">
				<div class="text-block">
					<h1 class="default-title has-decor"><?php _e("CORPORATE CULTURE" , "balfin")  ?></h1>
					<?php the_content(); ?>
					<!-- <a href="#" class="read-more">Read More</a> -->
				</div>
			</div>
		</div>
	</div>
</div>
<div class="section-vision vision-mission">
	<div class="grid-container full">  
		<div class="grid-x">  
			<div class="cell medium-1"></div>
			<div class="cell medium-5">
				<div class="text-holder">
					<div class="text-block">
						<?php if( have_rows('vision_mision') ): ?>
    						<?php while( have_rows('vision_mision') ): the_row();  ?>
							<div class="default-title has-decor"><?php the_sub_field('title'); ?></div>
							<p><?php the_sub_field('text'); ?></p>
 						<?php endwhile; ?>
						<?php endif; ?>


					</div>
				</div>
			</div>
			<div class="cell medium-1"></div>
			<div class="cell medium-5">
				<div class="big-image fixed">
					<img src="<?php bloginfo('template_url') ?>/img/mision.jpeg" alt="">
				</div>
			</div>
		</div>
	</div>
</div>


<div class="section-vision">
	<div class="grid-container full">  
		<div class="grid-x">  
			
			<div class="cell medium-5">
				<div class="big-image fixed">
					<img src="<?php bloginfo('template_url') ?>/img/vision-banner2.jpg" alt="">
				</div>
			</div>
			<div class="cell medium-1"></div>
			
			
			<div class="cell medium-5">
				<div class="text-holder">
					<div class="text-block">
						<?php if( have_rows('vision_text') ): ?>
    						<?php while( have_rows('vision_text') ): the_row();  ?>
							<div class="default-title has-decor"><?php the_sub_field('title'); ?></div>
							<p><?php the_sub_field('text'); ?></p>
 						<?php endwhile; ?>
						<?php endif; ?>


					</div>
				</div>
			</div>
			<div class="cell medium-1"></div>
		</div>
	</div>
</div>

<div class="section-vision section-core-values">
	<div class="grid-container full">
		<div class="grid-x align-center">   
			<div class="cell medium-1"></div>
			<div class="cell medium-10">
				<div class="text-holder">
					<div class="text-block">

						<?php if( have_rows('core_values') ): ?>
   						 <?php while( have_rows('core_values') ): the_row();  ?>

							<div class="default-title has-decor center extra-margin-bottom"><?php the_sub_field('title'); ?></div>

 						<?php endwhile; ?>
						<?php endif; ?>
						 
						<div class="grid-x grid-padding-x">
							<div class="medium-6 cell wow fadeInLeft">
									<?php if( have_rows('accountability') ): ?>
   									<?php while( have_rows('accountability') ): the_row();  ?>

								<p><img class="value-icon" style="width: 44px;" src="<?php bloginfo('template_url'); ?>/img/accountability-icon.svg" alt=""><strong>
				<?php the_sub_field('capital_title'); ?>  – <i><?php the_sub_field('italic_title'); ?></i></strong><br><?php the_sub_field('content'); ?></p>
							
									<?php endwhile; ?>
									<?php endif; ?>

							</div>
							<div class="medium-6 cell wow fadeInRight">
									<?php if( have_rows('partnership') ): ?>
   									<?php while( have_rows('partnership') ): the_row();  ?>


								<p><img class="value-icon" src="<?php bloginfo('template_url'); ?>/img/partnership-icon.svg" alt=""><strong><?php the_sub_field('capital_title'); ?> - <i>
				<?php the_sub_field('italic_title'); ?></i></strong><br><?php the_sub_field('content'); ?></p>

									<?php endwhile; ?>
									<?php endif; ?>

							</div>
							<div class="medium-6 cell wow fadeInLeft">
									<?php if( have_rows('innovation') ): ?>
   									<?php while( have_rows('innovation') ): the_row();  ?>

								<p><img class="value-icon" style="width: 44px;" src="<?php bloginfo('template_url'); ?>/img/innovation-icon.svg" alt=""><strong>
					<?php the_sub_field('capital_title'); ?>  – <i><?php the_sub_field('italic_title'); ?></i></strong><br><?php the_sub_field('content'); ?></p>

									<?php endwhile; ?>
									<?php endif; ?>
									
							</div>
							<div class="medium-6 cell wow fadeInRight">

									<?php if( have_rows('consideration') ): ?>
   									<?php while( have_rows('consideration') ): the_row();  ?>

								<p><img class="value-icon" src="<?php bloginfo('template_url'); ?>/img/consideration-icon.svg" alt="">
								<strong><?php the_sub_field('capital_title'); ?> - <i><?php the_sub_field('italic_title'); ?></i></strong><br><?php the_sub_field('content'); ?></p>
							
									<?php endwhile; ?>
									<?php endif; ?>
									
							</div>
						</div>
						<!-- <a href="<?php echo site_url() ?>/contact-us" class="read-more">Become Our Partner</a> -->






					</div>
				</div>
			</div>
			<div class="cell medium-1"></div>
		</div>
	</div>
</div>



	

<?php endwhile;endif; ?>
<?php get_footer(); ?>