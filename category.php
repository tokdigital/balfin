<?php get_header(); ?>


<?php 
$category = $wp_query->get_queried_object();
$category_slug = $category->slug;
$category_name = $category->name;
?>
<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs"><span><span><a href="https://balfin.al/"><?php _e("Home" , "balfin")  ?></a> &gt; <a href="https://balfin.al/news"><a href="https://balfin.al/"><?php _e("News" , "balfin")  ?></a> &gt; <span class="breadcrumb_last" aria-current="page"><?php echo $category_name; ?></span></span></span></p>
				</div>
			</div>
		</div>
	</div>	
</section>


<div class="section-all-posts">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-10">
				<div class="posts-wrapper">
					<?php if(isset($_GET['tag']))
						{
						   $tag = $_GET['tag'];
						   $args = array(
						 	'post_type' => 'post',
					        'posts_per_page' => 10,
					        'category_name' => $category_slug,
					        'tag' => $tag
					        );
						   echo do_shortcode('[ajax_load_more id="2747230537" post_type="post" posts_per_page="10" category="' . $category_slug . '" tag="' .$tag.'" scroll="false"]');
						} else {
							$args = array(
						 	'post_type' => 'post',
					        'posts_per_page' => 10,
					        'category_name' => $category_slug
					        );
						   echo do_shortcode('[ajax_load_more id="2741430537" post_type="post" posts_per_page="10" category="' . $category_slug . '"  scroll="false"]');
						}

					?> 
				</div>
			</div>
			<div class="cell medium-2">
				<div class="filter-holder">
					<?php echo do_shortcode( '[searchandfilter headings="Categories" types="radio,radio" fields="category" hide_empty="1"]' ); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>