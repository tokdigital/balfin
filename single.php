<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs"><span><span><a href="https://balfin.al/"><?php _e("Home" , "balfin")  ?></a> &gt; <a href="https://balfin.al/news"><?php _e("News" , "balfin")  ?></a> &gt; <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></p>
				</div>
			</div>
		</div>
	</div>	
</section>

<div class="section-single-post">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center">
			<div class="cell medium-8">
				<div class="post-content">
					<div class="date"><?php $post_date = get_the_date( 'F j, Y' ); echo $post_date; ?></div>
					<div class="default-title"><?php the_title(); ?></div>
					<div class="featured-image"><?php the_post_thumbnail(); ?></div>
					<?php the_content(); ?>
					<?php 
					$images = get_field('photo_slider');
					if( $images ):  $newcounter = 0; ?>
						<div class="single-news-slider">
			            <?php foreach( $images as $image ): ?> 
								<div>
									<img src="<?php echo esc_url($image['url']); ?>" alt="">
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<?php if (get_field('video_iframe')) { ?>
						<div class="video-holder">
							<?php the_field('video_iframe'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
			<!-- <div class="cell medium-4">
				<div class="news-with-photo in-single">
					<div class="category">Most Popular</div>
				<?php 
				 $args = array(
				 	'post_type' => 'post',
			        'posts_per_page' => 3,
		            'orderby'        => 'rand'
			        );
				    $loop = new WP_Query( $args );
					if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
				    	$categories = get_the_category();
						if ( ! empty( $categories ) ) {
						    $catName = $categories[0]->name;
						}
				     ?>
						<div class="single-news">
							<div class="grid-x">
								<div class="cell medium-6">
									<a class="image-holder" href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail(); ?>
									</a>
								</div>
								<div class="cell medium-6">
									<div class="content">
										<span class="title"><?php the_title(); ?></span><br>
										<p><?php echo substr(get_the_excerpt(), 0,40)."…"; ?></p>
										<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
									</div>
								</div>
							</div>
						</div> 
					<?php endwhile;endif;wp_reset_postdata(); ?>
				</div>
			</div> -->
		</div>
	</div>
</div>

	

<?php endwhile;endif; ?>
<?php get_footer(); ?>