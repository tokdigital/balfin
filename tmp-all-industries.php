<?php /* Template Name: All industries */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>
			</div>
		</div>
	</div>	
</section>

<div class="grid-container">
	<div class="grid-x">
		<div class="cell medium-12">
			<div class="big-title">
				<?php the_title(); ?>
			</div>
		</div>
		<div class="cell medium-12">
			<?php the_content(); ?>
		</div>
	</div>
</div>

<div class="section-all-industries">
	<div class="grid-container">
		<div class="grid-x grid-padding-x"> 

	<?php
		$industries = get_field('industries');
		if( $industries ): $counter = 1; $delay = 0; ?>

		    <?php foreach( $industries as $post ): 
		        setup_postdata($post); ?>
		        <div class="cell medium-4">
					<a class="single-block wow fadeInRight" <?php if ($delay < 0.5) { ?>data-wow-delay="<?php echo($delay); ?>s" <?php } else { $delay = 0;?>data-wow-delay="0s" <?php } ?> href="<?php the_permalink(); ?>">
						<div class="content-wrapper">
							<div class="title"><?php the_title(); ?></div>
							<div class="featured-image">
								<img src="<?php the_field('home_photo'); ?>" alt="">
							</div>
						</div>
					</a>
				</div>

		    <?php $delay=$delay + 0.2;$counter++; endforeach; ?>
		    <?php 
		    // Reset the global post object so that the rest of the page works correctly.
		    wp_reset_postdata(); ?>
		<?php endif; ?>
		</div>
	</div>
</div>

	

<?php endwhile;endif; ?>
<?php get_footer(); ?>