<?php get_header(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs"><span><span><a href="https://balfin.al/"><?php _e("Home" , "balfin")  ?></a></span></span></span></p>
				</div>
			</div>
		</div>
	</div>	
</section>

<div class="section-single-post">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-8">
				<div class="post-content">
					<div class="default-title">Error 404</div>
					<p><?php _e("Page not found. Go to " , "balfin")  ?><a href="<?php echo site_url(); ?>"><?php _e("HOMEPAGE" , "balfin")  ?></a>.</p>
				</div>
			</div>
			 
		</div>
	</div>
</div>

	

<?php get_footer(); ?>