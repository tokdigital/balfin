<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>

<div class="page-content">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
    		<div class="cell"><h4 class="default-title has-decor"><?php the_title(); ?></h4></div>
			<div class="cell">
				<div class="text">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</div>


	

<?php endwhile;endif; ?>
<?php get_footer(); ?>