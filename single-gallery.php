<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section style="padding-top: 30px;">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs"><span><span><a href="https://balfin.al/"><?php _e("Home" , "balfin")  ?></a> &gt; <a href="https://balfin.al/press-kit"><?php _e("Press Kit" , "balfin")  ?></a> &gt; <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></p>
				</div>
			</div>
		</div>
	</div>	
</section>
 
<div class="grid-container">
	<div class="grid-x">
		<div class="cell medium-12">
				<div class="default-title"><?php the_title(); ?></div>
				<?php if(have_rows('file_download')): ?>
					<div class="videos-gallery">
						<div class="grid-x grid-padding-x">
							<?php $counter=0; while(have_rows('file_download')):the_row(); ?> 
								<div class="cell medium-3"> 
									<div class="single-video">
										<a class="poster" href="<?php the_sub_field('file_zip'); ?>" download><img src="<?php the_sub_field('preview'); ?>" alt=""></a>
										<div class="buttons-holder">
											<div class="title"><?php the_sub_field('name') ?></div>
											<a class="download-button" href="<?php the_sub_field('file_zip'); ?>" download style="display:inline-block;width: 40px;flex: 1 0 40px;max-width: 40px;"><img style="margin-left: 0;" src="<?php bloginfo('template_url') ?>/img/download.svg"></a>
										</div>
									</div>
								</div>
							<?php $counter++;endwhile; ?>
						</div>
					</div>
				<?php endif; ?>

				<!-- <?php 
				$images = get_field('photo_gallery');
				if( $images ):  $newcounter = 0; ?>
					<div class="press_gallery photos <?php $newtitle = sanitize_title(get_the_title()); echo $newtitle; ?>">
						<div class="grid-x medium-up-5">
			            <?php 
			            $count = 0;
			            foreach( $images as $image ): ?> 
								<div class="cell">
									<div class="single-image">
										<a href="<?php echo esc_url($image['url']); ?>" target="_blank" class="photo-holder" data-title=""><img src="<?php echo esc_url($image['url']); ?>" alt=""></a>
										<div class="buttons-holder">
											<a href="<?php echo esc_url($image['url']); ?>" class="download-button" download>Download <img src="<?php bloginfo('template_url') ?>/img/download.svg"></a>
											<a href="<?php echo esc_url($image['url']); ?>" class="download-button view" data-lightbox="<?php echo 'x'.$count; ?>" data-title="">View</a>
										</div>
									</div>
								</div>
							<?php $count++;endforeach; ?>
						</div>
					</div>
				<?php endif; ?> -->

				<?php if(have_rows('video_gallery')): ?>
					<div class="videos-gallery">
						<div class="grid-x grid-padding-x">
							<?php $counter=0; while(have_rows('video_gallery')):the_row(); ?>
								<div class="cell medium-3">
									<?php if(get_sub_field('video_iframe')) { ?>
									<div class="single-video iframe">
										<?php the_sub_field('video_iframe'); ?>
										<div class="title"><?php the_sub_field('video_title') ?></div>
									</div>
									<?php } ?>
									<?php if(get_sub_field('video_file')){ ?>
									<div class="single-video">
										<div class="poster" data-video="<?php the_sub_field('video_file'); ?>"><img src="<?php the_sub_field('video_preview'); ?>" alt=""></div>
										<div class="title"><?php the_sub_field('video_title') ?></div>
										<div class="buttons-holder">
											<div class="download-button" data-video="<?php the_sub_field('video_file'); ?>">Play <img src="<?php bloginfo('template_url') ?>/img/play-button2.svg"></div>
											<a class="download-button" href="<?php the_sub_field('video_file'); ?>" download><?php _e("Download" , "balfin")  ?> <img src="<?php bloginfo('template_url') ?>/img/download.svg"></a>
										</div>
									</div>
									<?php } ?>
								</div>
							<?php $counter++;endwhile; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="video-popup">
		<div class="inner">
			<div class="close-button">&times;</div>
			<video src="" id="gallery-video" controls></video>
		</div>
	</div>

	

<?php endwhile;endif; ?>
<?php get_footer(); ?>