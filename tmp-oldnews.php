<?php /* Template Name: Media Center OLD */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>
			</div>
		</div>
	</div>	
</section>

<div class="section-main-news">
	<div class="grid-x">
		<div class="cell medium-2">
			<div class="page-title"><?php the_title(); ?></div>
		</div>
		<div class="cell medium-7">
			<div class="news-slider-holder">
				<div class="big-news-slider">
					<?php 
					 $args = array(
					 	'post_type' => 'post',
				        'posts_per_page' => 3
				        );
				    $loop = new WP_Query( $args );
				     ?>
					<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
					<div>
						<div class="single-news">
							<div class="grid-x grid-padding-x">
								<div class="cell medium-8">
									<a class="slider-link" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
								</div>
								<div class="cell medium-4">
									<div class="content">
										<div class="default-title has-decor"><?php the_title(); ?></div>
										<p><?php echo substr(get_the_excerpt(), 0,200)."…"; ?></p>
										<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php endwhile;endif;wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
		<div class="cell medium-2">
			<div class="small-news">
				<?php 
				 $args = array(
				 	'post_type' => 'post',
			        'posts_per_page' => 3,
			        'offset' => 4
			        );
			    $loop = new WP_Query( $args );
			     ?>
				<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
				<a href="<?php the_permalink(); ?>" class="single-news">
					<?php the_post_thumbnail(); ?>
					<span class="title"><?php the_title(); ?></span>
				</a>
				<?php endwhile;endif;wp_reset_postdata(); ?>
			</div>
		</div>
		<div class="cell medium-1">
		</div>
	</div>
</div>

<div class="section-socials">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell medium-3">
				
				<div class="single-social">
					<a href="https://www.instagram.com/balfingroup/" target="_blank" class="default-button">Follow</a>
					<a href="https://www.instagram.com/balfingroup/" target="_blank"><img src="<?php bloginfo('template_url') ?>/img/ig-colored.svg" alt=""></a>
				</div>
			</div>
			<div class="cell medium-3">
				<div class="single-social">
					<a href="https://www.facebook.com/Balfin.Group" target="_blank" class="default-button">Follow</a>
					<a href="https://www.facebook.com/Balfin.Group" target="_blank"><img src="<?php bloginfo('template_url') ?>/img/fb-colored.svg" alt=""></a>
				</div>
			</div>
			<div class="cell medium-3">
				<div class="single-social">
					<a href="https://www.linkedin.com/company/balfingroup" target="_blank" class="default-button">Follow</a>
					<a href="https://www.linkedin.com/company/balfingroup" target="_blank"><img src="<?php bloginfo('template_url') ?>/img/in-colored.svg" alt=""></a>
				</div>
			</div>
			<div class="cell medium-3">
				<div class="single-social">
					<a href="https://twitter.com/BalfinGroup" target="_blank" class="default-button">Follow</a>
					<a href="https://twitter.com/BalfinGroup" target="_blank"><img src="<?php bloginfo('template_url') ?>/img/twitter-colored.svg" alt=""></a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="section-text section-calendar">
	<div class="grid-container">
		<div class="grid-x">  
			<div class="cell medium-6">
				<div class="calendar-wrapper">
					<div class="single-event">
						<div class="date">
							<span class="number">06</span><span class="day">Thursday</span>
						</div>
						<div class="event-holder">
							<div class="time">16:00</div>
							<div class="location">Skanderbeg Square</div>
							<div class="event">Female Rights</div>
						</div>
					</div>
					<div class="single-event">
						<div class="date">
							<span class="number">06</span><span class="day">Thursday</span>
						</div>
						<div class="event-holder">
							<div class="time">16:00</div>
							<div class="location">Skanderbeg Square</div>
							<div class="event">Female Rights</div>
						</div>
					</div>
					<div class="single-event">
						<div class="date">
							<span class="number">06</span><span class="day">Thursday</span>
						</div>
						<div class="event-holder">
							<div class="time">16:00</div>
							<div class="location">Skanderbeg Square</div>
							<div class="event">Female Rights</div>
						</div>
					</div>
				</div>
			</div>
			<div class="cell medium-1"></div>
			<div class="cell medium-5">
				<div class="text-holder">
					<div class="text-block">
						<div class="default-title ">CALENDAR</div>
						<div class="divider-vertical"></div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
						<a href="#" class="read-more">Read More</a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="section-text gray section-multimedia">
	<div class="grid-container">
		<div class="grid-x">  
			<div class="cell medium-12">
				<div class="default-title  has-decor">MULTIMEDIA</div>
			</div>
		</div>
		<div class="grid-x grid-padding-x">
			<?php 
			 $args = array(
			 	'post_type' => 'post',
		        'posts_per_page' => 4,
		        'offset' => 7
		        );
		    $loop = new WP_Query( $args );
			if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
		    	$categories = get_the_category();
				if ( ! empty( $categories ) ) {
				    $catName = $categories[0]->name;
				}
		     ?>
			<div class="cell medium-3">
				<a href="<?php the_permalink() ?>" class="single-media">
					<?php the_post_thumbnail(); ?>
					<span class="category"><?php echo($catName); ?></span>
				</a>
				<a href="<?php the_permalink() ?>" class="single-media-title">
					<span class="title"><?php the_title(); ?></span>
				</a>
			</div> 
			<?php endwhile;endif;wp_reset_postdata(); ?>
		</div>
	</div>
</div>

<div class="section-other-news">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<?php 
			 $args = array(
			 	'post_type' => 'post',
		        'posts_per_page' => 1,
		        'offset' => 11
		        );
		    $loop = new WP_Query( $args );
			if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
		    	$categories = get_the_category();
				if ( ! empty( $categories ) ) {
				    $catName = $categories[0]->name;
				}
		     ?>
			<div class="cell medium-4">
				<a href="<?php the_permalink() ?>" class="single-big-news">
					<?php the_post_thumbnail(); ?>
					<span class="title"><?php the_title(); ?></span>
					<span class="category"><?php echo($catName); ?></span>
				</a>
			</div>
			<?php endwhile;endif;wp_reset_postdata(); ?>
			<div class="cell medium-4">
				<div class="news-with-photo">
				<?php 
				 $args = array(
				 	'post_type' => 'post',
			        'posts_per_page' => 3,
			        'offset' => 12
			        );
				    $loop = new WP_Query( $args );
					if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
				    	$categories = get_the_category();
						if ( ! empty( $categories ) ) {
						    $catName = $categories[0]->name;
						}
				     ?>
						<div class="single-news">
							<div class="grid-x">
								<div class="cell medium-6">
									<a class="image-holder" href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail(); ?>
									</a>
								</div>
								<div class="cell medium-6">
									<div class="content">
										<span class="category"><?php echo($catName); ?></span><br>
										<span class="title"><?php echo substr(get_the_title(), 0,40)."…"; ?></span><br>
										<p><?php echo substr(get_the_excerpt(), 0,40)."…"; ?></p>
										<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
									</div>
								</div>
							</div>
						</div> 
					<?php endwhile;endif;wp_reset_postdata(); ?>
				</div>
			</div>
			<div class="cell medium-4">
				<div class="news-with-photo no-photo">
				<?php 
				 $args = array(
				 	'post_type' => 'post',
			        'posts_per_page' => 4,
			        'offset' => 15
			        );
				    $loop = new WP_Query( $args );
					if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
				    	$categories = get_the_category();
						if ( ! empty( $categories ) ) {
						    $catName = $categories[0]->name;
						}
				     ?>
						<div class="single-news">
							<div class="content">
								<a href="<?php the_permalink(); ?>"><span class="title"><?php echo substr(get_the_excerpt(), 0,100)."…"; ?></span></a>
								<div class="date"><?php $post_date = get_the_date( 'l F j, Y' ); echo $post_date; ?></div>
							</div>
						</div>   
					<?php endwhile;endif;wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="section-text gray">
	<div class="grid-container">
		<div class="grid-x">  
			<div class="cell medium-6">
				<div class="image-wrapper">
					<img src="<?php bloginfo('template_url') ?>/img/gov/gov2.jpg" alt="">
				</div>
			</div>
			<div class="cell medium-1"></div>
			<div class="cell medium-5">
				<div class="text-holder">
					<div class="text-block">
						<div class="default-title">MEDIA CONTACTS</div>
						<div class="divider-vertical"></div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
						<a href="#" class="read-more">Read More</a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<?php endwhile;endif; ?>
<?php get_footer(); ?>