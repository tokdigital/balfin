<?php /* Template Name: Balfin Multimedia */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<section>
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="breadcrumbs">
							<p id="breadcrumbs">
								<span>
									<span>
										<a href="https://balfin.al/"> <?php _e("Home", "balfin")  ?> </a> &gt; <span> <?php _e("Media Center", "balfin")  ?> &gt; <span class="breadcrumb_last" aria-current="page"> <?php the_title(); ?> </span>
										</span>
									</span>
								</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php
		$args0 = array(
			'post_type' => 'multimedia',
			'posts_per_page' => -1,
			'multimedia-category' => 'balfin-shorts'
		);

		$loop1 = new WP_Query($args1);
		$loop0 = new WP_Query($args0);
		?>

		<div class="board-members-section">
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="default-title has-decor center">
							<?php the_title(); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="board-tabs">
							<?php if ($loop0->have_posts()) { ?>
								<div class="single-tab active" data-tab="0">
									<span>
										<?php _e("BALFIN Shorts", "balfin")  ?>
									</span>
								</div>
							<?php } ?>

							<?php if ($loop1->have_posts()) { ?>
								<div class="single-tab" data-tab="1">
									<span>
										<?php _e("Explorer Podcast", "balfin")  ?>
									</span>
								</div>
							<?php } ?>

						</div>

						<?php if ($loop0->have_posts()) { ?>
							<div class="board-content active" data-content="0">
								<div class="grid-x grid-padding-x">
									<div class="pub-shorts" style="padding-bottom:50px;">
										<section class="section-shorts">
											<div class="grid-container">
												<div class="grid-x">
													<div class="cell medium-3">
														<div class="text-holder wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
														</div>
													</div>
													<div class="cell medium-6" style="padding-top:8em;padding-bottom:8em">
														<h2 class="title">BALFIN Shorts</h2>
														<p class="shorts-desc">Get to know our people in a series of short videos capturing their drive and values in and out of the workplace. At BALFIN Group, we are proud to support a diverse workforce, whose dedication and passion remain key to our success story.</p>
														<!-- <div class="row">
													<div class="youtube-img">
														<a href="https://www.youtube.com/channel/UCfsqwU18xRnFUz6b2RE2bOQ"><img src="https://balfin.al/wp-content/uploads/2022/05/youtube.png" alt="Youtube"></a>
													</div>
												</div> -->
													</div>
													<div class="cell medium-3" style="padding-bottom:50px;">
														<div class="text-holder wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
														</div>
													</div>
												</div>
											</div>
										</section>
										<?php if ($loop0->have_posts()) : while ($loop0->have_posts()) : $loop0->the_post(); ?>
												<div class="single-short">
													<div class="grid-x y">
														<div class="cell medium-6" style="padding:20px">
															<div class="video-holder">
																<div class="embed-container">
																	<?php the_field('video_thumbnail'); ?>
																</div>
															</div>
														</div>
														<div class="cell medium-6 general-info" style="padding:20px">
															<div class="info">
																<p class="shorts_title "><?php the_field('shorts_title'); ?></p>
																<h3 class="video-title"><?php the_title(); ?></h3>
																<p class="shorts-description"><?php the_field('shorts-description'); ?></p>
															</div>
															<div class="bottom">
																<p class="duration"> Duration:
																	<?php the_field('shorts-duration'); ?>
																</p>
																<p class="listen"> Watch on:
																	<a href="https://www.youtube.com/channel/UCfsqwU18xRnFUz6b2RE2bOQ" target="_blank"><span><?php the_field('listen_on'); ?> </span></a>
																</p>
															</div>
														</div>
													</div>
												</div>
										<?php endwhile;
										endif;
										wp_reset_postdata(); ?>
									</div>
								</div>
							</div>
						<?php } ?>
				<?php endwhile;
		endif; ?>
					</div>
				</div>
			</div>
		</div>

		<?php get_footer(); ?>