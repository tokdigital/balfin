<?php /* Template Name: Contact */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell medium-12">
                <div class="breadcrumbs">
                    <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                      yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>  
</section>


<div class="section-new-contact"> 
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell medium-6">
                <div class="contact-info">
                    <div class="default-title "><?php the_title(); ?></div>
                    <?php 
                     $counter1 = 0;
                     if (have_rows('enquiry_types')) : while(have_rows('enquiry_types')) : the_row(); ?>

                         <?php if (have_rows('contact_blocks')) : while(have_rows('contact_blocks')) : the_row(); ?>
                            
                          <?php if ($counter1 < 1) { ?>
                            <div class="contact-single-block">
                                <div class="name"><?php the_sub_field('name'); ?></div>
                                <div class="content-row email"><label><?php _e("Email" , "balfin")  ?>:</label><div class="content"><a target="_blank" href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></div></div>
                                <div class="content-row mobile"><label><?php _e("Phone" , "balfin")  ?>:</label><div class="content"><a href="tel:<?php the_sub_field('mobile'); ?>" target="_blank"><?php the_sub_field('mobile'); ?></a></div></div>
                                <div class="content-row address"><label><?php _e("Address" , "balfin")  ?>:</label><div class="content"><a href="<?php the_sub_field('address_link') ?>" target="_blank"><?php the_sub_field('address'); ?></a></div></div> 
                            </div>
                          <?php } else { ?>
                            <div class="contact-single-block">
                                <div class="name"><?php the_sub_field('name'); ?></div>
                                <div class="content-row email"><label><?php _e("Email" , "balfin")  ?>:</label><div class="content"><a target="_blank" href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></div></div>
                            </div>
                        <?php } ?>
                        <?php endwhile;endif; ?>
                        <?php $counter1++;endwhile;endif; ?>
                        <div class="contact-single-block">
                            <div class="name"><?php _e('Social Medias') ?>
                                <div class="socials">
                                    <!-- <a target="_blank" href="https://www.linkedin.com/company/balfingroup"><img src="<?php bloginfo('template_url') ?>/img/in-colored.svg" alt=""></a>
                                    <a target="_blank" href="https://www.facebook.com/Balfin.Group"><img src="<?php bloginfo('template_url') ?>/img/fb-colored.svg" alt=""></a>
                                    <a target="_blank" href="https://www.instagram.com/balfingroup/"><img src="<?php bloginfo('template_url') ?>/img/ig-colored.svg" alt=""></a> -->

                                    <a target="_blank" href="https://www.linkedin.com/company/balfingroup">LinkedIn</a>
                                    <a target="_blank" href="https://www.facebook.com/Balfin.Group">Facebook</a>
                                    <a target="_blank" href="https://www.instagram.com/balfingroup/">Instagram</a>
                                    <!-- <a target="_blank" href="https://www.instagram.com/balfingroup/"><img src="<?php bloginfo('template_url') ?>/img/insta.svg" alt=""></a> -->
                                    <!-- <a target="_blank" href="https://twitter.com/BalfinGroup"><img src="<?php bloginfo('template_url') ?>/img/twitter.svg" alt=""></a> -->
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="cell medium-6">
              <div class="form-holder">
                  <?php echo do_shortcode('[ninja_form id=1]  '); ?>
              </div>
                
            </div>
            <!-- <div class="cell medium-12">
              <div class="big-map">
                    <div id="map0"></div>
                </div>
            </div> -->
        </div>
    </div>
</div>
 




	

<?php endwhile;endif; ?>
<?php get_footer(); ?>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDB63CSNJRcYHZQj7DlygHP6FfHDNLqm5Q&callback=initMap"
    async defer></script>
<script>
function initMap() {
  
    var isDraggable = $(document).width() > 640 ? true : false; // If document (your website) is wider than 480px, isDraggable = true, else isDraggable = false
    var centerPosition = {lat: 41.320173266192704, lng: 19.823116064071655};
    
    var map = new google.maps.Map(document.getElementById('map0'), {
        center: centerPosition,
        zoom: 16
    });

    // var map = new google.maps.Map(document.getElementById('map4'), {
    //     center: centerPosition,
    //     zoom: 16
    // });
    // var map = new google.maps.Map(document.getElementById('map2'), {
    //     center: centerPosition,
    //     zoom: 16
    // });
    // var map = new google.maps.Map(document.getElementById('map3'), {
    //     center: centerPosition,
    //     zoom: 16
    // });

    
    var marker = new google.maps.Marker({
        position: centerPosition
    });
    map.setOptions({
      draggable: isDraggable,
      scrollwheel: false
    });
    marker.setMap(map);

}
</script> 