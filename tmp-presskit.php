<?php /* Template Name: Press Kit */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs"><span><span><a href="https://balfin.al/"><?php _e("Home" , "balfin")  ?></a> &gt; <span><?php _e("Media Center" , "balfin")  ?> &gt; <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></span></p>
				</div>
			</div>
		</div>
	</div>	
</section>

<!-- <div class="section-text gray section-multimedia">
	<div class="grid-container">
		<div class="grid-x">  
			<div class="cell medium-12">
				<div class="default-title has-decor"><?php the_title(); ?></div>
				<?php the_content(); ?>
			</div>
		</div>
		<div class="grid-x grid-padding-x align-center">
			<?php 
		    $loop = new WP_Query( $args );
			if(have_rows('press_kit_files')) : while (have_rows('press_kit_files')) : the_row();
		    	 
		     ?>
			<div class="cell medium-4">
				<div class="single-press-kit">
					<div class="photo-holder">
						<?php if (get_sub_field('bg_image')){ ?>
							<img src="<?php the_sub_field('bg_image'); ?>" alt="">
							<div class="single-title"><?php the_sub_field('title') ?></div>
						<?php } ?>
					</div>
					<div class="button-holder">
						<?php if(have_rows('buttons')) : while(have_rows('buttons')):the_row(); ?>
							<a class="download-button" href="<?php the_sub_field('button_link') ?>"><?php the_sub_field('button_text') ?></a>
						<?php endwhile;endif; ?>
					</div>
				</div>
			</div>
			<?php endwhile;endif; ?>
		</div>
	</div>
</div> -->

<div class="board-members-section">
	<div class="grid-container">
		<div class="grid-x">  
			<div class="cell medium-12">
				<div class="default-title has-decor"><?php the_title(); ?></div>
				<?php the_content(); ?>
			</div>
		</div> 
	</div>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12"> 
				<div class="board-content active" data-content="0">
					<div class="grid-x grid-padding-x">
						<div class="pub-files">
							<?php 
						    $loop = new WP_Query( $args );
							if(have_rows('press_kit_files')) : while (have_rows('press_kit_files')) : the_row();
						    	 
						     ?> 
							<div class="single-file">
								<div class="title"><?php the_sub_field('title') ?></div>
								<div class="press-kit-buttons">
								<?php if(have_rows('buttons')) : while(have_rows('buttons')):the_row(); ?>
									<div class="download">
										<a class="download-button" href="<?php the_sub_field('button_link') ?>"><?php the_sub_field('button_text') ?></a>
									</div>	
								<?php endwhile;endif; ?> 
								</div>
							</div>
							<?php endwhile;endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php endwhile;endif; ?>
<?php get_footer(); ?>