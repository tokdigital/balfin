<?php /* Template Name: Industry Whole */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>
			</div>
		</div>
	</div>	
</section>

<section class="section-single-industry-first">
	<div class="grid-container full">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				<div class="text-holder">
					<div class="text-box">
						<h1 class="default-title"><?php the_title(); ?></h1>
						<!-- <div class="divider-vertical"></div> -->
						<?php the_content(); ?>
						<!-- <a href="#" class="read-more">Read More</a> -->
					</div>
				</div>
			</div>
			<div class="cell medium-6">
				<?php the_post_thumbnail(); ?>
			</div>
		</div>
	</div>
</section>


<?php 
	if( have_rows('industries_new') ):
 	$industriesCounter = 1;

    while( have_rows('industries_new') ) : the_row(); 
	 $newtitle = sanitize_title(get_sub_field('department_title'));
	?> 
	<?php if($industriesCounter % 2 == 0) { ?>
	<div class="section-single-industry-content <?php if($industriesCounter % 2 !== 0) {echo 'gray';} ?>" id="<?php echo($newtitle); ?>">
		<div class="single-wrapper">
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-3">
						<div class="sub-title"><?php the_sub_field('department_title') ?></div> 
					</div>
					<div class="cell medium-9">
						<div class="logos dev <?php echo($newtitle); ?>">
							<?php 
								$depName = get_sub_field('department_title');
								$depCounter = preg_replace('/[^A-Za-z0-9-]+/', '-', $depName);

								if( have_rows('department_industries') ):
								$logoCounter = 0;
								

				    			while( have_rows('department_industries') ) : the_row(); 
							?>
							<div class="single-logo <?php echo $depCounter; if($logoCounter == 0){echo ' active';} ?>" data-goto="<?php echo $logoCounter; ?>">
								<img src="<?php the_sub_field('industry_logo') ?>" alt="">
							</div>
							<?php $logoCounter++;endwhile;endif; ?>
							
						</div>
					</div>
					<div class="new-slider <?php echo $depCounter.'-slider'; ?>">
						<?php 
							if( have_rows('department_industries') ):
							$counter = 1;

			    			while( have_rows('department_industries') ) : the_row(); 
						?>
						<div>
							<div class="grid-x grid-padding-x"> 
								<div class="cell medium-6">
									<div class="text-holder">
										<div class="text-box"> 
											<div class="default-title">
												<?php the_sub_field('industry_title'); ?>
													<!-- <div class="gotonext-arrow dev">
														<img src="<?php bloginfo('template_url') ?>/img/arrow-right-red-thin.svg" alt="">
													</div> -->
												</div>
											<div class="logo-wrapper">
												<!-- <div class="divider-vertical"></div> -->
												<!-- <?php if (get_sub_field('logo')) {  ?>
													<?php if (get_sub_field('link')) {  ?>
													<a href="<?php the_sub_field('link'); ?>" target="_blank" class="logo-link">
														<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
													</a>
													<?php } else { ?>
														<div>
															<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
														</div>
													<?php } ?>
												<?php } ?> -->
											</div>
											<?php the_sub_field('industry_text'); ?>
											<?php if (get_sub_field('industry_link'))  { ?>
												<a target="_blank" href="<?php the_sub_field('industry_link'); ?>" class="read-more"><?php _e("Visit Website" , "balfin")  ?></a>
											<?php } ?>
										</div>
									</div>
								</div> 

								<div class="cell medium-6">
									<div class="photo-holder"> 
										<div class="single-photo"> 
											<div>
												<img src="<?php the_sub_field('industry_image') ?>" alt="">
											</div> 
										</div> 
									</div>
								</div>	
							</div>	
						</div>
						<?php $counter++;endwhile;endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } else { ?>
<div class="section-single-industry-content <?php if($industriesCounter % 2 !== 0) {echo 'gray';} ?>" id="<?php echo($newtitle); ?>">
		<div class="single-wrapper">
			<div class="grid-container">
				<div class="grid-x grid-padding-x">
					<div class="cell medium-3">
						<div class="sub-title"><?php the_sub_field('department_title') ?></div> 
					</div>
					<div class="cell medium-9">
						<div class="logos dev <?php echo($newtitle); ?>">
							<?php 
								$depName = get_sub_field('department_title');
								$depCounter = preg_replace('/[^A-Za-z0-9-]+/', '-', $depName);

								if( have_rows('department_industries') ):
								$logoCounter = 0;
								

				    			while( have_rows('department_industries') ) : the_row(); 
							?>
							<div class="single-logo <?php echo $depCounter; if($logoCounter == 0){echo ' active';} ?>" data-goto="<?php echo $logoCounter; ?>">
								<img src="<?php the_sub_field('industry_logo') ?>" alt="">
							</div>
							<?php $logoCounter++;endwhile;endif; ?>
							
						</div>
					</div>
					<div class="new-slider <?php echo $depCounter.'-slider'; ?>">
						<?php 
							if( have_rows('department_industries') ):
							$counter = 1;

			    			while( have_rows('department_industries') ) : the_row(); 
						?>
						<div>
							<div class="grid-x grid-padding-x">
								
									<div class="cell medium-6">
										<div class="photo-holder"> 
											<div class="single-photo"> 
												<div>
													<img src="<?php the_sub_field('industry_image') ?>" alt="">
												</div> 
											</div> 
										</div>
									</div>	 
									
									<div class="cell medium-6">
										<div class="text-holder">
											<div class="text-box"> 
												<div class="default-title">
													<?php the_sub_field('industry_title'); ?>
														<!-- <div class="gotonext-arrow dev">
															<img src="<?php bloginfo('template_url') ?>/img/arrow-right-red-thin.svg" alt="">
														</div> -->
													</div>
												<div class="logo-wrapper">
													<!-- <div class="divider-vertical"></div> -->
													<!-- <?php if (get_sub_field('logo')) {  ?>
														<?php if (get_sub_field('link')) {  ?>
														<a href="<?php the_sub_field('link'); ?>" target="_blank" class="logo-link">
															<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
														</a>
														<?php } else { ?>
															<div>
																<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
															</div>
														<?php } ?>
													<?php } ?> -->
												</div>
												<?php the_sub_field('industry_text'); ?>
												<?php if (get_sub_field('industry_link'))  { ?>
													<a target="_blank" href="<?php echo get_sub_field('industry_link'); ?>" class="read-more"><?php _e("Visit Website" , "balfin")  ?></a>
												<?php } ?>
											</div>
										</div>
									</div> 

							</div>	
						</div>
						<?php $counter++;endwhile;endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

<?php $industriesCounter++;endwhile;endif; ?>



	

<?php endwhile;endif; ?>
<?php get_footer(); ?>

<?php 
	if( have_rows('industries_new') ):
 
    while( have_rows('industries_new') ) : the_row(); 
    	$depName = get_sub_field('department_title');
		$depCounter = preg_replace('/[^A-Za-z0-9-]+/', '-', $depName);
     
		$ifSlider = 0;
		if( have_rows('department_industries') ):
		while( have_rows('department_industries') ) : the_row(); 
			$ifSlider++;
		endwhile;endif;

		if ($ifSlider > 0) {
	?>
    	<script>
			$('.<?php echo $depCounter; ?>-slider').slick({
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  autoplay: true,
			  autoplaySpeed: 4000,
			  arrows: true,
			  dots: false,
			  infinite: true,
			  swipeToSlide: false,
			  draggable: false,
			  speed: 1000
			}); 


			$('.gotonext-arrow.<?php echo $depCounter; ?>').click(function(){
			    $(".<?php echo $depCounter; ?>-slider").slick('slickNext');
			});

			$('.single-logo.<?php echo $depCounter; ?>').click(function(){
			    gotoNumber = $(this).attr('data-goto');
			    $(".<?php echo $depCounter; ?>-slider").slick('slickGoTo', gotoNumber);
			});

			$('.<?php echo $depCounter; ?>-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			  $('.single-logo.<?php echo $depCounter; ?>').removeClass('active');
			  $('.single-logo.<?php echo $depCounter; ?>[data-goto="'+nextSlide+'"]').addClass('active');
			})
    	</script>
<?php } endwhile; $ifSlider = 0;endif; ?>




