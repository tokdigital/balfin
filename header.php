<!doctype html>
<html class="no-js" lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php the_title(); ?> | BALFIN</title>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url') ?>/img/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_url') ?>/img/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url') ?>/img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_url') ?>/img/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url') ?>/img/fav/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo('template_url') ?>/img/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <script src='https://www.google.com/recaptcha/api.js'></script>
     <?php wp_head() ?>
     <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=G-83WBEJ0PF4"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-83WBEJ0PF4');
    </script> -->

     

    <!-- Global site tag (gtag.js) - Google Analytics -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100437483-1"></script>

    <script>

      window.dataLayer = window.dataLayer || [];

      function gtag(){dataLayer.push(arguments);}

      gtag('js', new Date());

     

      gtag('config', 'UA-100437483-1');

    </script>

     
  </head>
  <style>
        #wpadminbar {
            /*display: none !important;*/
        }
        html {
            /*margin-top: 0 !important;*/
        }
    </style>
  <body <?php body_class(); ?>>

     

    <div class="canvas-container">
        <div class="wrapper">
            <div class="grid-x">
                <div class="cell medium-6 hide-for-small-only">
                    <div class="close-overlay"></div>
                </div>
                <div class="cell medium-6">
                    <div class="big-menu">
                        <div class="button-holder">
                            <div class="menu-button">
                                <p>MENU</p>
                                <div class="icon">
                                    <img src="<?php bloginfo('template_url') ?>/img/close.svg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="menu-holder">
                            <div class="main-item has-submenu" data-selected="null" data-menu="1"><div class="inner"><?php _e("About Us" , "balfin")  ?></div></div>
                            <div class="main-item has-submenu" data-selected="null" data-menu="2"><div class="inner"><?php _e("Industries" , "balfin")  ?></div></div>
                            <!-- <div class="main-item is-link" data-selected="null" data-menu="3"><a href="<?php echo site_url(); ?>/partners" class="inner">Partners</a></div> -->
                            <div class="main-item is-link" data-selected="null" data-menu="4"><a href="<?php echo site_url(); ?>/human-resources" class="inner"><?php _e("Human Resources" , "balfin")  ?></a></div>
                            <div class="main-item is-link" data-selected="null" data-menu="5"><a href="<?php echo site_url(); ?>/sustainability/" class="inner"><?php _e("Sustainability" , "balfin")  ?></a></div>
                            <div class="main-item has-submenu" data-selected="null" data-menu="6"><div class="inner">News & Media</div></div>
                            <div class="main-item is-link" data-selected="null" data-menu="7"><a href="<?php echo site_url(); ?>/contacts" class="inner">Contacts</a></div>
                            <div class="single-menu-wrapper" menu="1">
                                <ul class="single-menu" >
                                    <li><a href="<?php echo site_url(); ?>/about-us/balfin-group/"><?php _e("BALFIN Group" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/president"><?php _e("President" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/governance"><?php _e("Governance" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/partnership/"><?php _e("Partnership" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/history"><?php _e("History" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/mission-vision-values"><?php _e("Mission, Vision & Values" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/financial-highlights"><?php _e("Financial Highlights" , "balfin")  ?></a></li>
                                    <li><div class="go-back-arrow"><img src="<?php bloginfo('template_url') ?>/img/arrow-left-white.svg" alt=""></div></li>
                                </ul>
                            </div>
                            <div class="single-menu-wrapper" menu="2">
                                <ul class="single-menu" >
                                    <li><a href="<?php echo site_url(); ?>/industries/wholesales-and-retail"><?php _e("Wholesales and Retail" , "balfin")  ?></a></li> 
                                    <li><a href="<?php echo site_url(); ?>/industries/real-estate"><?php _e("Real Estate" , "balfin")  ?></a></li> 
                                    <li><a href="<?php echo site_url(); ?>/industries/asset-management"><?php _e("Asset Managment" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/industries/banking"><?php _e("Banking" , "balfin")  ?></a></li> 
                                    <li><a href="<?php echo site_url(); ?>/industries/media"><?php _e("Media" , "balfin")  ?></a></li> 
                                    <li><a href="<?php echo site_url(); ?>/industries/logistics"><?php _e("Logistics" , "balfin")  ?></a></li> 
                                    <li><a href="<?php echo site_url(); ?>/industries/education"><?php _e("Education" , "balfin")  ?></a></li> 
                                    <!-- <li><a href="<?php echo site_url(); ?>/industries/tourism"><?php _e("Tourism" , "balfin")  ?></a></li>  -->
                                    <!-- <li><a href="<?php echo site_url(); ?>/industries/energy"><?php _e("Energy" , "balfin")  ?></a></li>  -->
                                    <li><div class="go-back-arrow"><img src="<?php bloginfo('template_url') ?>/img/arrow-left-white.svg" alt=""></div></li>
                                </ul>
                            </div>
                            <!-- <div class="single-menu-wrapper" menu="3">
                                <ul class="single-menu" >
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><div class="go-back-arrow"><img src="<?php bloginfo('template_url') ?>/img/arrow-left-white.svg" alt=""></div></li>
                                </ul>
                            </div>
                            <div class="single-menu-wrapper" menu="4">
                                <ul class="single-menu" >
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><div class="go-back-arrow"><img src="<?php bloginfo('template_url') ?>/img/arrow-left-white.svg" alt=""></div></li>
                                </ul>
                            </div>
                            <div class="single-menu-wrapper" menu="5">
                                <ul class="single-menu" >
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><div class="go-back-arrow"><img src="<?php bloginfo('template_url') ?>/img/arrow-left-white.svg" alt=""></div></li>
                                </ul>
                            </div>
                            <div class="single-menu-wrapper" menu="6">
                                <ul class="single-menu" >
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><div class="go-back-arrow"><img src="<?php bloginfo('template_url') ?>/img/arrow-left-white.svg" alt=""></div></li>
                                </ul>
                            </div>
                            -->
                            <div class="single-menu-wrapper" menu="6">
                                <ul class="single-menu" >
                                    <li><a href="<?php echo site_url(); ?>/news"><?php _e("News" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/press-kit"><?php _e("Press Kit" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/balfin-group-publication"><?php _e("Publications" , "balfin")  ?></a></li>
                                    <li><div class="go-back-arrow"><img src="<?php bloginfo('template_url') ?>/img/arrow-left-white.svg" alt=""></div></li>
                                    <li><a href="<?php echo site_url(); ?>/multimedia-balfin"><?php _e("Multimedia" , "balfin")  ?></a></li>
                                </ul>
                            </div> 
                        </div>
    
                        <!-- <?php do_action('wpml_add_language_selector'); ?> -->
    
                        <div class="socials-holder">
                            <div class="socials">
                                <a target="_blank" href="https://www.linkedin.com/company/balfingroup"><img src="<?php bloginfo('template_url') ?>/img/in.svg" alt=""></a>
                                <a target="_blank" href="https://www.facebook.com/Balfin.Group"><img src="<?php bloginfo('template_url') ?>/img/fb.svg" alt=""></a>
                                <a target="_blank" href="https://www.instagram.com/balfingroup/"><img src="<?php bloginfo('template_url') ?>/img/insta.svg" alt=""></a> 
                                <!-- <a target="_blank" href="https://twitter.com/BalfinGroup"><img src="<?php bloginfo('template_url') ?>/img/twitter.svg" alt=""></a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <div class="header-fix"></div>
    <div class="header wow fadeInDown" id="scroll-menu">
        <div class="main-navigation">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="medium-2 cell small-6">
                        <ul class="logo-container">
                            <li class="logo"><a href="<?php echo site_url() ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.svg"></a></li>
                        </ul> 
                    </div>   
                    <div class="cell medium-10 hide-for-small-only">
                        <div class="small-menu-holder">
                            <ul>
                                <li>
                                    <a href="<?php echo site_url(); ?>/contacts"><?php _e("Contacts" , "balfin")  ?></a>
                                </li>
                                <li class="header-search-icon">
                                    <img src="<?php bloginfo('template_url') ?>/img/search-icon.svg" alt="">
                                </li>
                            </ul>
                        </div>
                        <div class="menu-holder">
                            <?php 
                                wp_nav_menu(
                                array(
                                    'menu'=>'Main Menu'
                                )); 
                            ?>
                        </div>  
                    </div>
                    <div class="cell medium-2 text-right small-6 show-for-small-only">
                        <div class="menu-button">
                            <p>MENU</p>
                            <div class="icon">
                                <span class="bar"></span>
                                <span class="bar"></span>
                                <span class="bar"></span>
                                <span class="bar"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search-wrapper">
                <div class="inner">
                    <div class="close-search">
                        <img src="<?php bloginfo('template_url') ?>/img/close.svg" alt="">
                    </div>
                    <div class="grid-container">
                        <div class="grid-x align-center">
                            <div class="cell medium-7">
                                <div class="search-content">
                                    <div class="default-title has-decor">
                                        Search 
                                    </div>
                                    <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mega-menu" data-menu="about-us">
                <div class="grid-container">
                    <div class="grid-x align-center">
                        <div class="cell medium-9">
                            <div class="menu-title"><?php _e("About Us" , "balfin")  ?></div>
                        </div>
                    </div>
                    <div class="grid-x align-center">
                        <div class="cell medium-3">
                            <div class="block-wrapper">
                                <ul class="single-menu" >
                                    <li><a href="<?php echo site_url(); ?>/about-us/balfin-group/"><?php _e("BALFIN Group" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/president"><?php _e("President" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/governance"><?php _e("Governance" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/partnership/"><?php _e("Partnership" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/history"><?php _e("History" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/mission-vision-values"><?php _e("Mission, Vision & Values" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/about-us/financial-highlights"><?php _e("Financial Highlights" , "balfin")  ?></a></li> 
                                </ul>
                            </div>
                        </div>
                        <!-- <div class="cell medium-3">
                            <div class="block-wrapper ">
                                <div class="single-block">
                                    <a href="<?php echo site_url(); ?>/about-us/balfin-group/" class="block-title">
                                        Balfin Group
                                    </a>
                                    <div class="video-holder">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/nrXUBhYXJyM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="cell medium-6">
                            <div class="block-wrapper no-border">
                                <div class="single-block">
                                    <a href="<?php echo site_url(); ?>/about-us/president" class="block-title">
                                    <?php _e("Featured Content" , "balfin")  ?>
                                    </a>
                                    <a href="<?php echo site_url(); ?>/about-us/president" class="block-image">
                                        <img class="chairman-image" src="<?php bloginfo('template_url') ?>/img/chairman5.jpg" alt="">
                                        <!-- <img class="chairman-image" src="<?php bloginfo('template_url') ?>/img/samir-mane-balfin.jpg" alt=""> -->
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mega-menu" data-menu="industries">
                <div class="grid-container">
                    <div class="grid-x align-center">
                        <div class="cell medium-9">
                            <div class="menu-title"><?php _e("Industries" , "balfin")  ?></div>
                        </div>
                    </div>
                    <div class="grid-x align-center">
                        <div class="cell medium-3">
                            <div class="block-wrapper">
                                <ul class="single-menu" >
                                    <li><a href="<?php echo site_url(); ?>/industries/wholesales-and-retail"><?php _e("Wholesales and Retail" , "balfin")  ?></a></li> 
                                    <li><a href="<?php echo site_url(); ?>/industries/real-estate"><?php _e("Real Estate" , "balfin")  ?></a></li> 
                                    <li><a href="<?php echo site_url(); ?>/industries/asset-management"><?php _e("Asset Managment" , "balfin")  ?></a></li> 
                                    <li><a href="<?php echo site_url(); ?>/industries/banking"><?php _e("Banking" , "balfin")  ?></a></li> 
                                    <li><a href="<?php echo site_url(); ?>/industries/media"><?php _e("Media" , "balfin")  ?></a></li> 
                                    <li><a href="<?php echo site_url(); ?>/industries/logistics"><?php _e("Logistics" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/industries/education"><?php _e("Education" , "balfin")  ?></a></li> 
                                    <!-- <li><a href="<?php echo site_url(); ?>/industries/tourism"><?php _e("Tourism" , "balfin")  ?></a></li>  -->
                                    <!-- <li><a href="<?php echo site_url(); ?>/industries/energy"><?php _e("Energy" , "balfin")  ?></a></li>  -->
                                </ul>
                            </div>
                        </div>
                        <!-- <div class="cell medium-3">
                            <div class="block-wrapper">
                                <div class="single-block">
                                    <a href="<?php echo site_url(); ?>/industries/real-estate" class="block-title">
                                        Real Estate
                                    </a>
                                    <a href="<?php echo site_url(); ?>/industries/real-estate" class="block-image">
                                        <img src="https://balfin.al/wp-content/uploads/2020/10/mane-slider1.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div> -->
                        <div class="cell medium-6">
                            <div class="block-wrapper no-border">
                                <div class="single-block">
                                    <a href="<?php echo site_url(); ?>/industries/wholesales-and-retail" class="block-title">
                                    <?php _e("Featured Content" , "balfin")  ?> 
                                    </a>
                                    <a href="<?php echo site_url(); ?>/industries/wholesales-and-retail" class="block-image">
                                        <img src="https://balfin.al/wp-content/uploads/2021/08/02.-Wholesale-Retail.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mega-menu" data-menu="media-center">
                <div class="grid-container">
                    <div class="grid-x align-center">
                        <div class="cell medium-9">
                            <div class="menu-title"><?php _e("News & Media" , "balfin")  ?></div>
                        </div>
                    </div>
                    <div class="grid-x align-center">
                        <div class="cell medium-3">
                            <div class="block-wrapper">
                                <ul class="single-menu" >
                                    <li><a href="<?php echo site_url(); ?>/news"><?php _e("News" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/press-kit"><?php _e("Press Kit" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/balfin-group-publication"><?php _e("Publications" , "balfin")  ?></a></li>
                                    <li><a href="<?php echo site_url(); ?>/multimedia-balfin"><?php _e("Multimedia" , "balfin")  ?></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- <div class="cell medium-3">
                            <div class="block-wrapper ">
                                <div class="single-block">
                                    <a href="<?php echo site_url(); ?>/industries/real-estate" class="block-title">
                                        Featured News
                                    </a>
                                    <a href="<?php echo site_url(); ?>/industries/real-estate" class="block-image"> 
                                    </a>
                                </div>
                            </div>
                        </div> -->
                        <div class="cell medium-6">
                            <div class="block-wrapper no-border">
                            <div class="single-block">
                                <?php 
                                    $args = array(
                                        'post_type' => 'post',
                                        'posts_per_page' => 1
                                        );
                                    $loop = new WP_Query( $args );
                                ?>
                                <?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
                                    <a href="<?php the_permalink(); ?>" class="block-title">
                                    <?php _e("Featured Content" , "balfin")  ?> 
                                    </a>
                                    <a href="<?php the_permalink(); ?>" class="block-image">
                                        <?php the_post_thumbnail(); ?>
                                    </a>
                                    <?php endwhile;endif;wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
       