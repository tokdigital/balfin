<?php /* Template Name: Home2 */ ?>
<?php get_header(); ?>

<section class="section-hero  wow fadeIn" data-wow-delay="0.4s" data-wow-duration="1s">
	<div class="grid-container full">
		<div class="hero-frame">
			<div 
				id="gl" 
				data-imageOriginal="<?php bloginfo('template_url') ?>/img/hero1.jpg" 
				data-imageDepth="<?php bloginfo('template_url') ?>/img/herobg.jpg" 
				data-horizontalThreshold="35" 
				data-verticalThreshold="15">
			</div> 
			<div class="hero-slider">
				<div>
					<div class="single-slide">
						<div class="gotonext-hero">
							<img src="<?php bloginfo('template_url') ?>/img/arrow-right-white.svg" alt="">
						</div>
						<div class="text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus, sunt!</div>
					</div>
				</div>
				<div>
					<div class="single-slide">
						<div class="gotonext-hero">
							<img src="<?php bloginfo('template_url') ?>/img/arrow-right-white.svg" alt="">
						</div>
						<div class="text">Delectus at harum porro maxime? Facilis, maiores perspiciatis, iure iste blanditiis, possimus cupiditate cumque similique laborum architecto mollitia modi.</div>
					</div>
				</div>
				<div>
					<div class="single-slide">
						<div class="gotonext-hero">
							<img src="<?php bloginfo('template_url') ?>/img/arrow-right-white.svg" alt="">
						</div>
						<div class="text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus, sunt!</div>
					</div>
				</div>
				<div>
					<div class="single-slide">
						<div class="gotonext-hero">
							<img src="<?php bloginfo('template_url') ?>/img/arrow-right-white.svg" alt="">
						</div>
						<div class="text">Delectus at harum porro maxime? Facilis, maiores perspiciatis, iure iste blanditiis, possimus cupiditate cumque similique laborum architecto mollitia modi.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="section-about">
	<div class="grid-container full">
		<div class="grid-x">
			<div class="cell medium-4">
				<div class="text-holder wow fadeInLeft">
					<div class="text-box">
						<div class="default-title">About the Company</div>
						<p>Balfin Group, the biggest investment group in Albania and the strongest in Balkan is distinguished for large investments in construction field. </p>
						<a href="#" class="read-more">Read More</a>
					</div>
				</div>
			</div>
			<div class="cell medium-8">
				<img src="<?php bloginfo('template_url') ?>/img/about-banner.jpg" alt="" class="wow fadeInRight" style="z-index: -1;">
			</div>
		</div>
	</div>
</section>

<section class="section-group">
	<div class="grid-container ">
		<div class="grid-x">
			<div class="cell medium-5">
				<div class="text-holder">
					<div class="text-box wow fadeInLeft">
						<div class="grid-x">
							<div class="cell medium-12">
								<div class="default-title">
									Balfin <br>Group
									<div class="arrow-holder">
										<div class="gotonext-industries"><img src="<?php bloginfo('template_url') ?>/img/arrow-right-red-thin.svg" alt=""></div>
									</div>
								</div>
								<p>Balkan Finance Investment Group <br>është një ndër grupet private investuese <br>më të mëdha në Shqipëri dhe rajon.</p>
								<a class="read-more" href="<?php echo site_url(); ?>/about-us/balfin-group/">Read More</a>
							</div> 
						</div>
					</div>
				</div>
			</div>
			<div class="cell medium-7">
				<div class="group-blocks">
					<div class="grid-x">
						<div class="cell medium-12">
							<div class="industries-slider">
								<?php
								$industries = get_field('industries');
								if( $industries ): $counter = 1; $delay = 0; ?>

								    <?php foreach( $industries as $post ): 
								        setup_postdata($post); ?>
								        <div>
											<div class="single-block wow fadeInRight" <?php if ($delay < 0.8) { ?>data-wow-delay="<?php echo($delay); ?>s"<?php } ?>>
												<div class="first-wrapper">
													<div class="gray-title">
														<span class="zero"><img src="<?php bloginfo('template_url') ?>/img/zero.svg" alt=""></span><span class="number"><?php echo $counter; ?></span>
													</div>
												<div class="category"><?php the_title(); ?></div>
												</div>
												<div class="content-wrapper">
													<div class="featured-image">
														<img src="<?php the_field('home_photo'); ?>" alt="">
													</div>
													<div class="content">
														<div class="logo-row">
															<?php 
															if( have_rows('industry_rows') ):
														    while( have_rows('industry_rows') ) : the_row(); ?>
															<?php if (get_sub_field('logo')) {  ?>
																<?php if (get_sub_field('link')) {  ?>
																<a href="<?php the_sub_field('link'); ?>" target="_blank">
																	<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
																</a>
																<?php } else { ?>
																	<a href="<?php the_permalink(); ?>">
																		<img src="<?php the_sub_field('logo') ?>" alt="" class="logo">
																	</a>
																<?php } ?>
															<?php } endwhile;endif;?>
														</div>
														<p><?php echo substr(get_the_excerpt(), 0,220)."…"; ?></p>
														<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
													</div>
												</div>
											</div>
										</div>

								    <?php $delay=$delay + 0.25;$counter++; endforeach; ?>
								    <?php 
								    // Reset the global post object so that the rest of the page works correctly.
								    wp_reset_postdata(); ?>
								<?php endif; ?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-stats">
	<div class="grid-container">
		<div class="grid-padding-x grid-x grid-margin-x">
			<!-- <div class="cell medium-4">
				<div class="single-stat">
					<div class="number">5000</div>
					<p><span>Over 5000 people</span> <br>work in our Group companies.</p>
				</div>
			</div>
			<div class="cell medium-4">
				<div class="single-stat">
					<div class="number">500</div>
					<p>Group Yearly Turnover is more than <br><span>500 million Euros.</span></p>
				</div>
			</div>
			<div class="cell medium-4">
				<div class="single-stat">
					<div class="number">1.2</div>
					<p>Group Total Assets are more than <br><span>1.2 billion USD.</span></p>
				</div>
			</div> -->
			<div class="cell medium-4">
				<a href="<?php echo site_url(); ?>/about-us/balfin-group/" class="single-stat-photo stat1 wow fadeInRight" data-wow-delay="0s">
					<img src="<?php bloginfo('template_url') ?>/img/stat1text.svg" class="text" alt="">
					<img src="<?php bloginfo('template_url') ?>/img/stat1image.png" class="image" alt="">
				</a>
			</div>
			<div class="cell medium-4">
				<a href="<?php echo site_url(); ?>/about-us/balfin-group/" class="single-stat-photo stat2 wow fadeInRight" data-wow-delay="0.4s">
					<img src="<?php bloginfo('template_url') ?>/img/stat2text.svg" class="text" alt="">
					<img src="<?php bloginfo('template_url') ?>/img/stat2image.png" class="image" alt="">
				</a>
			</div>
			<div class="cell medium-4">
				<a href="<?php echo site_url(); ?>/about-us/balfin-group/" class="single-stat-photo stat3 wow fadeInRight" data-wow-delay="0.8s">
					<img src="<?php bloginfo('template_url') ?>/img/stat3text.svg" class="text" alt="">
					<img src="<?php bloginfo('template_url') ?>/img/stat3image.png" class="image" alt="">
				</a>
			</div>
		</div>
	</div>
</section>


<section class="section-news">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12"><div class="default-title has-decor">Latest</div></div>
		</div>
		<div class="news-slider wow fadeInUp">
			<?php 
			 $args = array(
			 	'post_type' => 'post',
		        'posts_per_page' => 10
		        );
		    $loop = new WP_Query( $args );
		     ?>
			<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
			<div>
				<div class="single-news">
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail(); ?>
					</a>
					<div class="content">
						<a href="<?php the_permalink(); ?>">
							<span class="news-title"><?php echo substr(get_the_title(), 0,40)."…"; ?></span>
						</a>
						<p><?php echo substr(get_the_excerpt(), 0,120)."…"; ?></p>
						<a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
					</div>
				</div>
			</div>
			
			 <?php endwhile;endif;wp_reset_postdata(); ?>
		</div>
	</div>
</section>


<section class="section-careers">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				<div class="image-wrapper wow fadeInLeft">
					<img src="<?php bloginfo('template_url') ?>/img/careers-banner.jpg" alt="">
				</div>
			</div>
			<div class="cell medium-1">
			</div>
			<div class="cell medium-5">
				<div class="content wow fadeInRight">
					<div class="default-title">Careers</div>
					<div class="divider-vertical"></div>
					<p>We are among the largest employers in the private sector in Albania. We are one of the biggest employers in the private sector in Albania. In Balfin Group companies work over 5000 individuals. The positioning our group has in the Albanian labor market makes it a very important player and an example to follow.</p>
					<a href="#" class="read-more">Contact Us</a>
				</div>
			</div>
		</div>
	</div>
</section>





































<?php get_footer(); ?>

<script src="<?php bloginfo('template_url') ?>/js/app2.js"></script>