<?php /* Template Name: Career OPs */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>
			</div>
		</div>
	</div>	
</section>

<!-- 

<div class="grid-container">
	<div class="grid-x">
		<div class="cell medium-12">
			<div class="big-title">
				<?php the_title(); ?>
			</div>
		</div>
	</div>
</div> -->

<div class="section-text no-pad-top">
	<div class="grid-container">
		<div class="grid-x">  
			<div class="cell medium-12">
				<div class="text-holder">
					<div class="text-block">
						<div class="default-title has-decor"><?php the_title(); ?></div>
						<?php the_content(); ?> 
					</div>
				</div>
			</div> 
		</div>
	</div>
</div>

<div class="job-openings-section">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="default-title has-decor f30"><?php _e("Job Openings" , "balfin")  ?></div>
			</div>
			<!-- <link rel="stylesheet" href="https://css.zohostatic.eu/recruit/embed_careers_site/css/v1.0/embed_jobs.css" type="text/css">
			<div class="embed_jobs_head embed_jobs_with_style_1 embed_jobs_with_style">
				<div class="embed_jobs_head2">
					<div class="embed_jobs_head3">
						<div id="rec_job_listing_div"> </div>
						<script type="text/javascript" src="https://js.zohostatic.eu/recruit/embed_careers_site/javascript/v1.0/embed_jobs.js"></script>
						<script type="text/javascript">
						rec_embed_js.load({
							widget_id:"rec_job_listing_div",
							page_name:"Careers",
							source:"CareerSite",
							site:"https://balfin.zohorecruit.eu",
							empty_job_msg:"No current Openings"
						});
						</script>
					</div>
				</div>
			</div> -->
			<div class="jobs">
				<?php 
				 $args = array(
				 	'post_type' => 'job',
			 	    'orderby' => 'date',
					'order', 'ASC',
			        'posts_per_page' => -1
			        );
			    $loop = new WP_Query( $args );
			    ?>
			    <?php 
			    	if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); 
			    	$post_id = get_the_ID();
			    	$company = get_the_terms($post_id,'job-company');
					$companyName = $company[0]->name; 
					$location = get_the_terms($post_id,'job-location');
					$locationName = $location[0]->name; 
		     		$post = get_post($post_id); 
					$slug = $post->post_name;

					$today = date('d/m/Y');
					$dead= get_field('deadline');
					$deadline = DateTime::createFromFormat('d/m/Y', $dead);
					$dateToday = DateTime::createFromFormat('d/m/Y', $today);
					if (get_field('deadline')) {
						if ($deadline < $dateToday) {} else {
		     	?>
				<div class="single-job wow fadeInLeft single-page"> 
					<div class="content">
						<a href="<?php the_permalink(); ?>"><div class="title small"><?php the_title(); ?></div></a>
						<div class="infos">
							<div class="info">Company: <strong><?php echo $companyName; ?></strong></div>
							<div class="info">Location: <strong><?php echo $locationName; ?></strong></div> 
							<?php 
								

								// echo ($today. '-' .$dead);
							 ?>
							<div class="info"><?php _e("Deadline" , "balfin")  ?>: <strong><?php echo $deadline->format('j M Y'); ?></strong></div> 
						</div> 
					</div>
					<?php if (get_field('link_zoho')) { ?>
					<a href="<?php the_field('link_zoho'); ?>" target="_blank" class="default-button"><?php _e("Apply Now" , "balfin")  ?></a>
					<?php } else { ?>
					<a href="<?php the_permalink(); ?>" target="_blank" class="default-button"><?php _e("Apply Now" , "balfin")  ?></a>
					<?php } ?>
				</div> 
				<?php } } else { ?>
					<div class="single-job wow fadeInLeft single-page"> 
						<div class="content">
							<a href="<?php the_permalink(); ?>"><div class="title small"><?php the_title(); ?></div></a>
							<div class="infos">
								<div class="info">Company: <strong><?php echo $companyName; ?></strong></div>
								<div class="info">Location: <strong><?php echo $locationName; ?></strong></div> 
								<?php 
								
								
								// echo ($today. '-' .$dead);
								?>
						</div> 
					</div>
					<?php if (get_field('link_zoho')) { ?>
						<a href="<?php the_field('link_zoho'); ?>" target="_blank" class="default-button"><?php _e("Apply Now" , "balfin")  ?></a>
						<?php } else { ?>
							<a href="<?php the_permalink(); ?>" target="_blank" class="default-button"><?php _e("Apply Now" , "balfin")  ?></a>
							<?php } ?>
						</div> 
				<?php } ?>
						
						
						
						
				<?php endwhile;endif;wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</div>









<?php endwhile;endif; ?>
<?php get_footer(); ?>