<?php /* Template Name: Financial Highlights */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs">
					<span><span><a href="https://balfin.al/"><?php _e("Home" , "balfin")  ?></a> &gt; <span><span><?php _e("About Us" , "balfin")  ?></span> &gt; <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></span>
					</p>
				</div>
			</div>
		</div>
	</div>	
</section>

<div class="grid-container">
	<div class="grid-x">
		<div class="cell medium-12">
			<h1 class="big-title">
				<?php the_title(); ?>
			</h1>
		</div>
	</div>
</div>

<div class="section-text">
	<div class="grid-container">
		<div class="grid-x">   
			<div class="cell medium-12">
				<div class="text-holder">
					<div class="text-block">
						<!-- <div class="default-title has-decor"><?php the_sub_field('section_title'); ?></div> -->
						<!-- <?php the_content(); ?> -->
						<div class="grid-x grid-padding-x">
							<div class="cell medium-6">
						<!-- <div class="default-title has-decor"></div> -->
								<?php the_content(); ?>
							</div>
							<div class="cell medium-6">
								<img src="<?php bloginfo('template_url') ?>/img/financial-banner.jpg" alt="" style="margin-bottom: 50px;">
							</div>

							<!-- <div class="cell medium-12">
								<br>
								<div class="default-title has-decor">
								<?php _e("Key Financial Highlights" , "balfin")  ?>
								</div>
							</div>
							<div class="grid-x medium-up-4 text-center">
								<div class="cell"><p class="single-financ"><strong>€482 M</strong><br>
								<?php _e("Consolidated Revenue" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ"><strong>€24 M</strong><br>
								<?php _e("Consolidated Net Profit" , "balfin")  ?>	</p></div>

								<div class="cell"><p class="single-financ"><strong>€77 M</strong><br>
								<?php _e("Consolidated EBITDA" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ"><strong>0.8%</strong><br>
								<?php _e("Return on Assets" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ"><strong>2.6%</strong><br>
								<?php _e("Return on Equity" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ"><strong>1.7x</strong><br>
								<?php _e("Net Debt / EBITDA" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ"><strong>2.5x</strong><br>
								<?php _e("Debt to Equity" , "balfin")  ?>	</p></div>

								<div class="cell"><p class="single-financ"><strong>31%</strong><br>
								<?php _e("Gross Margin" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ"><strong>€1.5 B</strong><br>
								<?php _e("Total Group Assets" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ" style="font-size: 16px;"><strong>€755 M</strong><br>
								<?php _e("Non-Consolidated Gross Revenue" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ"><strong>15,000</strong><br>
								<?php _e("Social Contributions" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ"><strong>€86 M</strong><br>
								<?php _e("Group EBITDA" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ"><strong>+ 6.300</strong><br>
								<?php _e("Employees in our Group" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ"><strong>€ 101 M </strong><br>
								<?php _e("Capex expenditures" , "balfin")  ?> </p></div>

								<div class="cell"><p class="single-financ"><strong>€ 59 M</strong><br>
								<?php _e("Taxes paid" , "balfin")  ?></p></div>

								<div class="cell"><p class="single-financ"><strong>+ 58 </strong><br>
								<?php _e("Companies In BALFIN Group" , "balfin")  ?></p></div>

							</div> -->
							<!-- <div class="cell medium-12">
								


<p>Real estate industry highly depends on sales recognition of the sold units of the main projects such as Vala Mar, Rolling Hills, Park Gate and Green Coast. The Group launched a new real estate project in Austria for two residential areas with three-store buildings located in a well-known touristic area.</p>



<p>The biggest real estate project in Skopje, North Macedonia is Skopje East Gate, with a total land area of 143 000 sqm, is rapidly being developed. The development will take place in three separate phases: shopping mall, residential buildings and office park.</p>


<p>Overall the retail industry continues to expend and facing optimistic growth.</p>


<p>KidZone network opened a new shop in Podgorica, Montenegro during 2019 and additional expansion in Bosnia. Neptun electronic retail is maintaining the market position in operating countries offering the best brands of home appliances and home entertainment devices. During 2019, the supermarket chain Spar Albania expanded the food retail with new markets in the country. In September 2019 Spar Albania purchased the supermarket chain Hippo, opening the doors of 7 new supermarkets in Tirana.</p>



<p>Both shopping centers, QTU and TEG, continued to offer the best shopping experience in Albania with renovated QTU and TEG with new brands and services.</p>



<p>The companies operating in mining industry, mainly Alb- Chrome in Albania and NewCo Ferronikeli in Kosovo are continuing the investments in order to increase the production and improve the technological processes and the general conditions.</p>



<p>Looking ahead, Balfin Group is progressing in the right direction to achieve stability, profitability and growth. The same pace of growth shall be maintained for the coming year and consolidating its position as one of the most powerful Groups not only in Albania, but in the entire region.</p>



<p>Balfin Group aim is to continue the expansion in other regions and acquire potential business.</p>
					
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="section-files"> -->
<div class="board-members-section">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">   
			<!-- <div class="cell medium-12">
				 <div class="default-title has-decor">Annual Reports</div>
			</div>
			<?php if(have_rows('annual_reports')) : while (have_rows('annual_reports')) : the_row(); ?>
				<div class="cell medium-3">
					<div class="single-file">
						<?php if (get_sub_field('cover')){ ?>
							<a target="_blank" href="<?php the_sub_field('file') ?>" class="photo">
								<img src="<?php the_sub_field('cover') ?>" alt="">
							</a>
						<?php } else { ?>
							<a target="_blank" href="<?php the_sub_field('file') ?>" class="icon">
								<img src="<?php bloginfo('template_url') ?>/img/pdf.svg" alt="">
							</a>
						<?php } ?>
						<a target="_blank" href="<?php the_sub_field('file') ?>" class="title">
							<?php the_sub_field('title') ?>
						</a>
					</div>
				</div>
			<?php endwhile;endif; ?> -->

			<div class="cell medium-12">
				<div class="default-title has-decor"><?php _e("Annual Reports" , "balfin")  ?></div>
			</div>
			<div class="cell medium-6">
				<div class="annual-image"><img src="https://balfin.al/wp-content/uploads/2022/11/Balfin_Annual_Report_2021_pantone-01-scaled.jpg" alt=""></div>
			</div>
			<div class="cell medium-6">
				<!-- <div class="section-new-numbers financial"> -->
					<div class="new-numbers-wrapper fadeInRight wow">
						<!-- <div class="new-numbers"> -->
						<!-- <?php $counter = 0; if(have_rows('annual_reports')) : while (have_rows('annual_reports')) : the_row(); ?>
								<div class="single-number-accordion">
									<div class="title <?php if ($counter == 0) {echo "";} ?>" data-acc-open="<?php echo($counter); ?>">
										<?php the_sub_field('title') ?>
									</div>
									<div class="content <?php if ($counter == 0) {echo "";} ?>" data-acc-content="<?php echo($counter); ?>">
										<a target="_blank" href="<?php the_sub_field('file') ?>" class="sub-title">
											<img src="<?php the_sub_field('cover') ?>" alt="">
										</a> 
										<a href="<?php the_sub_field('file') ?>" download target="_blank" alt="Download" class="with-icon"><img src="<?php bloginfo('template_url') ?>/img/download.svg" alt=""></a>
									</div>
								</div>
						<?php $counter++;endwhile;endif; ?> -->

							<?php
							 $args1 = array(
						 	'post_type' => 'file',
						    'posts_per_page' => -1,
						    'file-category' => 'annual-report'
						    );
							$loop1 = new WP_Query( $args1 );
							 if($loop1->have_posts()) { ?>
								<div class="board-content active" data-content="1">
									<div class="grid-x grid-padding-x">
										<div class="pub-files">
											<?php if($loop1->have_posts()) : while ($loop1->have_posts()) : $loop1->the_post(); ?>
											<div class="single-file">
												<div class="title">
													<a href="<?php the_field('upload_file') ?>" title="Download File" target="_blank">
														<?php the_title(); ?>
													</a>
												</div>
												<div class="download">
													<a href="<?php the_field('upload_file') ?>" title="Download File" download>
														<img src="<?php bloginfo('template_url') ?>/img/download.svg" alt="">
													</a>
												</div>	
											</div>
											<?php endwhile;endif;wp_reset_postdata(); ?>
										</div>
									</div>
								</div>
							<?php } ?>
						<!-- </div> -->
					</div>
				<!-- </div> -->
			</div>
		</div>
	</div>
</div>



			


	

<?php endwhile;endif; ?>
<?php get_footer(); ?>