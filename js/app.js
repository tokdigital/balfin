$(document).foundation();

$(".show-canvas").click(function () {
  $(".off-canvas-menu").animate({ left: "0" }, 300);
  $(".canvas-container").css("overflow", "visible");
  $("body").toggleClass("overflow");
  $(".header").css("overflow", "visible");
});
$(".close-canvas , .off-canvas-menu li a").click(function () {
  $(".off-canvas-menu").animate({ left: "100%" }, 300);
  $(".canvas-container").css("overflow", "hidden");
  $("body").toggleClass("overflow");
  $(".header").css("overflow", "hidden");
});

$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function (event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $("html, body").animate(
          {
            scrollTop: target.offset().top,
          },
          1000,
          function () {
            // Callback after animation
            // Must change focus!
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) {
              // Checking if the target was focused
              return false;
            } else {
              $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
              $target.focus(); // Set focus again
            }
          }
        );
      }
    }
  });

$(".news-slider").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 4000,
  arrows: true,
  dots: false,
  infinite: false,
  swipeToSlide: true,
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});
$(".news-slider2").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 4000,
  arrows: true,
  dots: false,
  infinite: false,
  swipeToSlide: true,
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});

$(".news-slider").on(
  "afterChange",
  function (event, slick, currentSlide, nextSlide) {
    console.log(currentSlide);
    if (currentSlide === 0) {
      $(".news-slider .slick-prev").css({
        opacity: "0",
        "pointer-events": "none",
      });
    } else if (currentSlide === 6) {
      $(".news-slider .slick-next").css({
        opacity: "0",
        "pointer-events": "none",
      });
    } else {
      $(".news-slider .slick-next,.news-slider .slick-prev").css({
        opacity: "1",
        "pointer-events": "all",
      });
    }
  }
);

$(".big-news-slider").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 4000,
  arrows: false,
  dots: true,
  infinite: true,
});

$(".industries-slider").slick({
  slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: false,
  arrows: false,
  dots: false,
  swipeToSlide: true,
  infinite: true,
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});

$(".history-slider").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 4000,
  arrows: false,
  dots: false,
  infinite: true,
  asNavFor: ".history-dots",
});

$(".history-dots").slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  centerMode: true,
  autoplay: false,
  autoplaySpeed: 4000,
  arrows: false,
  dots: false,
  infinite: false,
  focusOnSelect: true,
  // asNavFor: '.history-slider',
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 3,
      },
    },
  ],
});

$(".history-dots").on("wheel", function (e) {
  e.preventDefault();

  if (e.originalEvent.deltaY < 0) {
    $(this).slick("slickNext");
  } else {
    $(this).slick("slickPrev");
  }
});

$(".new-hero").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  autoplay: true,
  autoplaySpeed: 13000,
  arrows: false,
  dots: true,
  infinite: true,
});

$(".new-hero .slick-dots li").on("mouseover", function () {
  $(this).click();
});

$(".gotonext-hero").click(function () {
  $(".new-hero").slick("slickNext");
});
$(".gotonext").click(function () {
  $(".history-slider").slick("slickNext");
});

$(".gotonext-industries").click(function () {
  $(".industries-slider").slick("slickNext");
});
$(".gotoprev-industries").click(function () {
  $(".industries-slider").slick("slickPrev");
});

$(".single-slider").slick({
  slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: false,
  arrows: true,
  dots: false,
  infinite: true,
  nextArrow: '<div class="slick-next"></div>',
  prevArrow: '<div class="slick-prev"></div>',
});

$(".sports-slider").on(
  "beforeChange",
  function (event, slick, currentSlide, nextSlide) {
    $("[data-goto]").removeClass("active");
    $('[data-goto="' + nextSlide + '"]').addClass("active");
  }
);

$("[data-goto]").on("click", function () {
  gotoNumber = $(this).attr("data-goto");
  $("[data-goto]").removeClass("active");
  $(this).addClass("active");
  $(".sports-slider").slick("slickGoTo", gotoNumber);
});

$(".dev-slider").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 4000,
  arrows: true,
  dots: false,
  infinite: true,
  speed: 1000,
});

$(".gotonext-arrow.dev").click(function () {
  $(".dev-slider").slick("slickNext");
});

$(".single-logo.dev").click(function () {
  gotoNumber = $(this).attr("data-goto");
  $(".dev-slider").slick("slickGoTo", gotoNumber);
});
$(".dev-slider").on(
  "beforeChange",
  function (event, slick, currentSlide, nextSlide) {
    $(".single-logo.dev").removeClass("active");
    $('.single-logo.dev[data-goto="' + nextSlide + '"]').addClass("active");
  }
);

$(".man-slider").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 4000,
  arrows: true,
  dots: false,
  infinite: true,
  speed: 1000,
});

$(".gotonext-arrow.man").click(function () {
  $(".man-slider").slick("slickNext");
});

$(".single-logo.man").click(function () {
  gotoNumber = $(this).attr("data-goto");
  $(".man-slider").slick("slickGoTo", gotoNumber);
});
$(".man-slider").on(
  "beforeChange",
  function (event, slick, currentSlide, nextSlide) {
    $(".single-logo.man").removeClass("active");
    $('.single-logo.man[data-goto="' + nextSlide + '"]').addClass("active");
  }
);

$(".shop-slider").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 4000,
  arrows: true,
  dots: false,
  infinite: true,
  speed: 1000,
});

$(".single-news-slider").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 4000,
  arrows: true,
  dots: false,
  infinite: true,
  speed: 1000,
});

$(".gotonext-arrow.shop").click(function () {
  $(".shop-slider").slick("slickNext");
});

$(".single-logo.shop").click(function () {
  gotoNumber = $(this).attr("data-goto");
  $(".shop-slider").slick("slickGoTo", gotoNumber);
});

$(".shop-slider").on(
  "beforeChange",
  function (event, slick, currentSlide, nextSlide) {
    $(".single-logo.shop").removeClass("active");
    $('.single-logo.shop[data-goto="' + nextSlide + '"]').addClass("active");
  }
);

$(".header .menu-button").on("click", function () {
  $(".canvas-container").addClass("open");
});

$(".canvas-container .menu-button,.close-overlay,.main-item.is-link").on(
  "click",
  function () {
    $(".canvas-container").removeClass("open");
    // $('.menu-holder .main-item').attr('data-selected', 'null').attr('style','');
    // $('.single-menu-wrapper').removeClass('open');
  }
);

$(".menu-holder .has-submenu").on("click", function () {
  menuNumber = $(this).attr("data-menu");
  $(".single-menu-wrapper[menu]").removeClass("open");
  $(".single-menu-wrapper[menu=" + menuNumber + "]").addClass("open");
  $(".menu-holder .main-item").attr("style", "");
  $(".menu-holder .main-item").attr("data-selected", "false");
  $(this).attr("data-selected", "true");
  const blurredItems = document.querySelectorAll("[data-selected='false']");
  for (var i = 0; i < blurredItems.length; i++) {
    n = 30;
    console.log(n * i);
    blurredItems[i].style.cssText =
      "top:calc(" + n * i + "px);right:0;left: auto;transform: none;";
  }
});

$(".go-back-arrow").on("click", function () {
  $(".menu-holder .main-item").attr("data-selected", "null").attr("style", "");
  $(".single-menu-wrapper").removeClass("open");
});

check = 0;
var lastScrollTop = 0;
window.addEventListener("scroll", function () {
  // controls = $('.controls-holder');
  // controlsOffsetTop = $('.building-holder-wrapper').offset().top;
  // controlsOffsetBottom = $('.controls-holder').offset().bottom;
  // distanceTop     = (controlsOffsetTop - scrollTop);

  scrollTop = $(window).scrollTop();
  footer = document.getElementById("footer");

  var footerTop = footer.offsetTop;
  var fullHeight = $(window).height();
  var windowTop = window.pageYOffset;
  var windowBottom = windowTop + fullHeight;
  var st = window.pageYOffset || document.documentElement.scrollTop;

  if (st > lastScrollTop) {
    $(".header").addClass("scrolled");
    // $('.mega-menu').removeClass('open');
  } else {
    $(".header").removeClass("scrolled");
  }
  lastScrollTop = st <= 0 ? 0 : st;

  if ($(".chairman").length) {
    sectionHeight = $(".chairman").height();
    image = $("#chairman-image");
    // console.log(sectionHeight);

    if (sectionHeight - 155 > fullHeight) {
      image.addClass("abs");
    } else {
      if (windowBottom >= footerTop) {
        newbot = footerTop - windowBottom;
        // console.log(newbot);
        image.css({ transform: "translateY(" + newbot + "px)" });
      } else {
        image.css("transform", "none");
        image.removeClass("abs");
      }
    }
  }

  if ($(".why-us").length) {
    why = document.getElementById("why-us");
    var whyTop = why.offsetTop;
    if (scrollTop + fullHeight > whyTop + 50) {
      if (check == 0) {
        check = 1;
        var myCircle = Circles.create({
          id: "circles-1",
          radius: 60,
          value: 1.2,
          maxValue: 2.3,
          width: 20,
          text: function (value) {
            return value + "+";
          },
          colors: ["#ffffff", "#E50B0A"],
          duration: 2000,
          wrpClass: "circles-wrp",
          textClass: "circles-text",
          valueStrokeClass: "circles-valueStroke",
          maxValueStrokeClass: "circles-maxValueStroke",
          styleWrapper: true,
          styleText: true,
        });

        var myCircle1 = Circles.create({
          id: "circles-2",
          radius: 60,
          value: 5000,
          maxValue: 7000,
          width: 20,
          text: function (value) {
            return value + "+";
          },
          colors: ["#ffffff", "#E50B0A"],
          duration: 2000,
          wrpClass: "circles-wrp",
          textClass: "circles-text",
          valueStrokeClass: "circles-valueStroke",
          maxValueStrokeClass: "circles-maxValueStroke",
          styleWrapper: true,
          styleText: true,
        });

        var myCircle2 = Circles.create({
          id: "circles-3",
          radius: 60,
          value: 9,
          maxValue: 14,
          width: 20,
          text: function (value) {
            return value + "+";
          },
          colors: ["#ffffff", "#E50B0A"],
          duration: 2000,
          wrpClass: "circles-wrp",
          textClass: "circles-text",
          valueStrokeClass: "circles-valueStroke",
          maxValueStrokeClass: "circles-maxValueStroke",
          styleWrapper: true,
          styleText: true,
        });
        var myCircle3 = Circles.create({
          id: "circles-4",
          radius: 60,
          value: 500,
          maxValue: 650,
          width: 20,
          text: function (value) {
            return value + "+";
          },
          colors: ["#ffffff", "#E50B0A"],
          duration: 2000,
          wrpClass: "circles-wrp",
          textClass: "circles-text",
          valueStrokeClass: "circles-valueStroke",
          maxValueStrokeClass: "circles-maxValueStroke",
          styleWrapper: true,
          styleText: true,
        });
      }
    }
  }

  if (scrollTop > 100) {
    $(".header").addClass("sticky");
    $(".menu-button").addClass("sticky");
  } else {
    $(".header").removeClass("sticky");
    $(".menu-button").removeClass("sticky");
  }
});

$("[data-open]").on("click", function () {
  number = $(this).attr("data-open");
  $('[data-open="' + number + '"]').toggleClass("open");
  $('[data-content="' + number + '"]').toggleClass("open");
});

$("[data-acc-open]").on("click", function () {
  number = $(this).attr("data-acc-open");
  $("[data-acc-open]").removeClass("open");
  $('[data-acc-open="' + number + '"]').addClass("open");
  $("[data-acc-content]").removeClass("open");
  $('[data-acc-content="' + number + '"]').addClass("open");
});

$("[data-open-contact]").on("click", function () {
  number = $(this).attr("data-open-contact");
  $("[data-open-contact]").removeClass("open");
  $('[data-open-contact="' + number + '"]').addClass("open");
  $("[data-content-contact]").removeClass("open");
  $('[data-content-contact="' + number + '"]').addClass("open");
});

$("[data2-open]").on("click", function () {
  number = $(this).attr("data2-open");
  $("[data2-open]").removeClass("open");
  $('[data2-open="' + number + '"]').addClass("open");
  $("[data2-content]").removeClass("open");
  $('[data2-content="' + number + '"]').addClass("open");
});

$("[data2-close]").on("click", function () {
  number = $(this).attr("data2-close");
  $('[data2-content="' + number + '"]').removeClass("open");
  console.log(number);
});
$("[data-hr]").on("click", function () {
  gotoNumber = $(this).attr("data-hr");
  $("[data-popup]").removeClass("open");
  $("[data-popup=" + gotoNumber + "]").addClass("open");
});
$("[data-member]").on("click", function () {
  gotoNumber = $(this).attr("data-member");
  $("[data-popup]").removeClass("open");
  $("[data-popup=" + gotoNumber + "]").addClass("open");
});
$("[data-popup] .close-popup,[data-popup] .overlay").on("click", function () {
  $("[data-popup]").removeClass("open");
});

$(document).keyup(function (e) {
  if (e.key === "Escape") {
    $("[data-popup]").removeClass("open");
    $(".search-wrapper").removeClass("open");
  }
});

$(".board-tabs [data-tab]").on("click", function () {
  gotoNumber = $(this).attr("data-tab");
  $(".board-tabs [data-tab]").removeClass("active");
  $(this).addClass("active");
  $(".board-content[data-content]").removeClass("active");
  $(".board-content[data-content=" + gotoNumber + "]").addClass("active");
});

$(".searchandfilter input[type=radio]").on("click", function () {
  $(".searchandfilter").submit();
});

$(document).ready(function () {
  new WOW().init();
  setTimeout(function () {
    $("#rec_job_listing_div a").attr("target", "_blank");
  }, 1000);
});

$("#Path_7294").on("click", function () {
  $("[data-state]").removeClass("active");
  $(".single-logo").addClass("deactive");
  $(".single-logo.Bosnia-and-Herzegovina").removeClass("deactive");
  $("#state-holder").addClass("active");
  $("#state").text("Bosnia and Herzegovina");
});

$("#Path_7396").on("click", function () {
  $("[data-state]").removeClass("active");
  $(".single-logo").addClass("deactive");
  $(".single-logo.Montenegro").removeClass("deactive");
  $("#state-holder").addClass("active");
  $("#state").text("Montenegro");
});

$("#Path_7382").on("click", function () {
  $("[data-state]").removeClass("active");
  $(".single-logo").addClass("deactive");
  $(".single-logo.Kosovo").removeClass("deactive");
  $("#state-holder").addClass("active");
  $("#state").text("Kosovo");
});

$("#Path_7400").on("click", function () {
  $("[data-state]").removeClass("active");
  $(".single-logo").addClass("deactive");
  $(".single-logo.Macedonia").removeClass("deactive");
  $("#state-holder").addClass("active");
  $("#state").text("Macedonia");
});

$("#Path_7284").on("click", function () {
  $("[data-state]").removeClass("active");
  $(".single-logo").addClass("deactive");
  $(".single-logo.Albania").removeClass("deactive");
  $("#state-holder").addClass("active");
  $("#state").text("Albania");
});

$("#Path_7289").on("click", function () {
  $("[data-state]").removeClass("active");
  $(".single-logo").addClass("deactive");
  $(".single-logo.Austria").removeClass("deactive");
  $("#state-holder").addClass("active");
  $("#state").text("Austria");
});

$("#Path_7416").on("click", function () {
  $("[data-state]").removeClass("active");
  $(".single-logo").addClass("deactive");
  $(".single-logo.Netherlands").removeClass("deactive");
  $("#state-holder").addClass("active");
  $("#state").text("Netherlands");
});

$(".csr-popup .close-button").on("click", function () {
  $(".csr-popup").addClass("closed");
});

$("[data-video]").on("click", function () {
  videoUrl = $(this).attr("data-video");
  $("#gallery-video").attr("src", videoUrl);
  $(".video-popup").addClass("open");
  $("#gallery-video").get(0).play();
});

$(".video-popup .close-button").on("click", function () {
  $(".video-popup").removeClass("open");
  $("#gallery-video").get(0).pause();
});

$(".menu li").on("mouseover", function () {
  // $('.mega-menu').removeClass('open');
});

$(".mega-menu").on("mouseout", function () {
  $(".mega-menu").removeClass("open");
});

$("#menu-item-2813").on("mouseover", function () {
  $(".mega-menu").removeClass("open");
  $('.mega-menu[data-menu="about-us"]').addClass("open");
});

$("#menu-item-2807").on("mouseover", function () {
  $(".mega-menu").removeClass("open");
  $('.mega-menu[data-menu="industries"]').addClass("open");
});

$("#menu-item-2814").on("mouseover", function () {
  $(".mega-menu").removeClass("open");
  $('.mega-menu[data-menu="media-center"]').addClass("open");
});

$("#menu-item-2813,#menu-item-2807,#menu-item-2814").on(
  "mouseout",
  function () {
    // $('.mega-menu').removeClass('open');
  }
);

$(".header-search-icon").on("click", function () {
  $(".search-wrapper").toggleClass("open");
});

$(".close-search").on("click", function () {
  $(".search-wrapper").removeClass("open");
});
