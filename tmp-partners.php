<?php /* Template Name: Partners */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>
			</div>
		</div>
	</div>	
</section>

<div class="grid-container">
	<div class="grid-x">
		<div class="cell medium-12">
			<h1 class="big-title">
				<?php the_title(); ?>
			</h1>
		</div>
	</div>
</div>


<div class="section-mission partners">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-6">
				<div class="text-block-holder" style="padding: 20px 0;">

<?php if( have_rows('partnership_paragraphs') ): ?>

    <?php while( have_rows('partnership_paragraphs') ): the_row();  ?>



					<div class="text-block">
						<!-- <div class="default-title"> <?php the_title(); ?>  Intro</div> -->
						<p>	
							<?php the_sub_field('paragraph1'); ?><br><br>
							<?php the_sub_field('paragraph2'); ?><br><br>
							<?php the_sub_field('paragraph3'); ?>
 						</p>
						<!-- </div> -->
						<!-- <div class="read-more" data-open="1"><span class="readmore">Show More</span><span class="readless">Show Less</span></div> -->


	<?php endwhile; ?>

<?php endif; ?>



					</div>
				</div>
			</div>
			<div class="cell medium-6">
				<div class="big-image">
					<?php the_post_thumbnail(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
 
<?php if( have_rows('flex_content') ):

    // Loop through rows.
    while ( have_rows('flex_content') ) : the_row();
    	$counter = 100;

        // Case: Paragraph layout.
        if( get_row_layout() == 'section_text' ): ?>

			<div class="section-text">
				<div class="grid-container">
					<div class="grid-x">   
						<div class="cell medium-12">
							<div class="text-holder">
								<div class="text-block">
									<div class="default-title has-decor"><?php the_sub_field('section_title'); ?></div>
									<?php the_sub_field('section_text'); ?>
									<?php if (get_sub_field('read_more_text')) { ?>
										
									<div class="hidden-content" data-content="<?php echo $counter; ?>">
				 						<?php the_sub_field('read_more_text'); ?>
				 					</div>
									<div class="read-more" data-open="<?php echo $counter; ?>"><span class="readmore"><?php _e("Show More" , "balfin")  ?></span><span class="readless"><?php _e("Show Less" , "balfin")  ?></span></div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php  elseif ( get_row_layout() == 'section_gray' ):?>
			<div class="section-vision">
				<div class="grid-container full">  
					<div class="grid-x">  
						<div class="cell medium-1"></div>
						<div class="cell medium-5">
							<div class="text-holder">
								<div class="text-block">
									<div class="default-title has-decor"><?php the_sub_field('section_title'); ?></div>
									<?php if(get_sub_field('section_sub_title')){ ?>
										<p><strong><?php the_sub_field('section_sub_title'); ?></strong></p>
									<?php } ?>
									<?php the_sub_field('section_text'); ?>
								</div>
							</div>
						</div>
						<div class="cell medium-1"></div>
						<div class="cell medium-5">
							<div class="big-image">
								<img src="<?php the_sub_field('section_image'); ?>" alt="" style="max-height: 580px;height: 100%;">
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php  elseif ( get_row_layout() == 'section_partners' ):?>
			
			<div class="section-text">
				<div class="grid-container">
					<div class="grid-x">   
						<div class="cell medium-12">
							<div class="text-holder">
								<div class="text-block">
									<div class="default-title has-decor"><?php the_sub_field('section_title'); ?></div>
									<?php the_sub_field('section_content'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="section-logos">
					<div class="grid-container">
						<div class="grid-x">
							<?php 
							if (have_rows('companies')) { 
								while (have_rows('companies')) { 
									the_row();
							?>
								<div class="cell medium-3">
									<div class="logo-wrapper">
										<a href="<?php the_sub_field('link'); ?>" target="_blank" style="cursor: default;" onClick="event.preventDefault()">
											<img src="<?php the_sub_field('logo'); ?>" alt="">
										</a>
									</div>
								</div>
							<?php 
								} 
							}
							?>
						</div>
					</div>
				</div> 
			</div>


<?php endif;$counter++;endwhile;endif; ?>


	

<?php endwhile;endif; ?>
<?php get_footer(); ?>