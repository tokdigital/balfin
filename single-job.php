<?php get_header(); 
?>

<?php 
	if(have_posts()) : while (have_posts()) : the_post(); 
	$post_id = get_the_ID(); 
?>
<?php 
	$post_id = get_the_ID();
	$company = get_the_terms($post_id,'job-company'); 
	$companyName = $company[0]->name; 
	$location = get_the_terms($post_id,'job-location');
	$locationName = $location[0]->name; 
	$post = get_post($post_id); 
	$slug = $post->post_name;
 ?>


<div class="single-job-page">
	<div class="grid-x align-center">
		<div class="cell medium-8">
			<div class="single-job wow fadeInLeft single-page"> 
				<div class="content">
					<div class="title small"><?php the_title(); ?></div>
					<div class="infos">
						<div class="info"><strong><?php _e("Company" , "balfin")  ?>: </strong><?php echo $companyName; ?></div>
						<div class="info"><strong><?php _e("Location" , "balfin")  ?>: </strong><?php echo $locationName; ?></div> 
						<?php 
							$today = date('d/m/Y');
							$dead= get_field('deadline');
							$deadline = DateTime::createFromFormat('d/m/Y', $dead);

							// echo ($today. '-' .$dead);
						 ?>
						<?php  if (get_field('deadline')) { ?>
						<div class="info"><strong><?php _e("Deadline" , "balfin")  ?>: </strong><?php echo $deadline->format('j M Y'); ?></div> 
						<?php  } ?>
					</div>
					<br>
					<div class="description">
						<?php the_content(); ?>
						<br>  
						<?php if (get_field('link_zoho')) { ?>
						<a href="<?php the_field('link_zoho'); ?>" target="_blank" class="default-button"><?php _e("Apply Now" , "balfin")  ?></a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>

 

 <?php endwhile;endif;wp_reset_postdata(); ?>

<?php get_template_part( 'subscribe' ); ?>






































<?php get_footer(); ?>