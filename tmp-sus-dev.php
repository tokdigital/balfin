<?php /* Template Name: Sustainable Development */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


		<section>
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="breadcrumbs">
							<?php
							if (function_exists('yoast_breadcrumb')) {
								yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="grid-container">
			<div class="grid-x">
				<div class="cell medium-12">
					<div>
						<h1 class="big-title"><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</div>



		<div class="section-text no-pad-top">
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-6">
						<div class="text-holder">
							<div class="text-block">
								<!-- <div class="default-title has-decor"><?php _e("Home", "balfin")  ?>Approach</div> -->
								<p>
									<?php _e("BALFIN Group works to meet its objectives by adhering to its values: Accountability, Partnership, Innovation, Consideration. In compliance with these values and the internal Code of Conduct, as well as according to international best practices, BALFIN Group has established four pillars of corporate
									responsibility: Education, Health and Well-Being, Environment, and Poverty Alleviation.", "balfin")  ?>
								</p>
								<p>
									<?php _e("Companies of BALFIN Group have long since been active with projects benefiting society. Considering their respective sectors and the geographical reach of their activity, they are focused on several directions and act as representatives of Group’s Corporate Social Responsibility. Their projects are built around these pillars.", "balfin")  ?>
								</p>

								<div class="read-more" data-open="approach"><span class="readmore"><?php _e("Show More", "balfin")  ?></span></div>
							</div>
						</div>
					</div>
					<div class="cell medium-6">
						<div class="image-wrapper" style="padding-bottom: 20px;">
							<img src="<?php bloginfo('template_url') ?>/img/balfin-group-sustainibility-social.jpg" alt="sustainable">
						</div>
					</div>
					<div class="cell medium-12">
						<div class="hidden-content" data-content="approach">
							<p>
								<?php _e("BALFIN Group is a signatory of Women’s Empowerment Principles, re-affirming its dedication to take bigger and faster steps to empower women in the workplace and community. ", "balfin")  ?>
							</p>
							<p>
								<?php _e("BALFIN Group plays its role to achieve the Sustainable Development Goals applicable to its fi elds of activity. Many of the products our companies offer, solutions we present and measures we take contribute to various groups of society, the education and healthcare system, environment, and workplace standards, making BALFIN Group part of the implementation of these goals.", "balfin")  ?>
							</p>
							<p>
								<?php _e("The long-term projects, as well as focused initiatives of BALFIN Group’s companies, have reached thousands of people over the years. Be it in cooperation with established organizations, as part of our dedicated operations, or even ideas launched by employees, BALFIN Group is committed to becoming a force of change in the corporate social responsibility landscape of the countries where it operates.", "balfin")  ?>
							</p>

							<div class="read-more" data-open="approach"><span class="readless"><?php _e("Show Less", "balfin")  ?></span></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- <div class="section-text gray">
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="default-title center has-decor"><?php _e("Our key pillars in social programs", "balfin")  ?></div>
					</div>
				</div>
				<div class="grid-x align-center">
					<div class="cell medium-2">
						<div class="single-sus-icon">
							<div class="icon">
								<img src="<?php bloginfo('template_url') ?>/img/icons/sus-new1.svg" alt="">
							</div>
							<div class="title"><?php _e("Health & Well-being", "balfin")  ?></div>
						</div>
					</div>
					<div class="cell medium-2">
						<div class="single-sus-icon">
							<div class="icon">
								<img src="<?php bloginfo('template_url') ?>/img/icons/sus-new2.svg" alt="">
							</div>
							<div class="title"><?php _e("Education", "balfin")  ?></div>
						</div>
					</div>
					<div class="cell medium-2">
						<div class="single-sus-icon">
							<div class="icon">
								<img src="<?php bloginfo('template_url') ?>/img/icons/sus-new3.svg" alt="">
							</div>
							<div class="title"><?php _e("Poverty Alleviation", "balfin")  ?></div>
						</div>
					</div>
					<div class="cell medium-2">
						<div class="single-sus-icon">
							<div class="icon">
								<img src="<?php bloginfo('template_url') ?>/img/icons/sus-new4.svg" alt="">
							</div>
							<div class="title"><?php _e("Environment", "balfin")  ?></div>
						</div>
					</div>
					<div class="cell medium-10">
						<p>
							<?php _e("In the light of modern globalization issues and environmental protection strategies, it is our special mission to consider these topics by assessing any potential impact during project implementation.<br><br>
							We have received the Philanthropy Award from Partners Albania for its great work in social responsibility.", "balfin")  ?>
						</p>
					</div>
				</div>
			</div>
		</div> -->

		<section class="section-news sus">
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="default-title center has-decor wow fadeInUp"><?php _e("Latest Social Programs", "balfin")  ?></div>
					</div>
				</div>
				<div class="news-slider2 wow fadeInUp">
					<?php
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => -1,
						'category_name' => 'social-program'
					);
					$loop = new WP_Query($args);
					?>
					<?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
							<div>
								<div class="single-news with-shadow">
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail(); ?>
									</a>
									<div class="content">
										<a href="<?php the_permalink(); ?>">
											<?php if (get_field('preview_title')) { ?>
												<span class="news-title"><?php the_field('preview_title'); ?></span>
											<?php } else { ?>
												<span class="news-title"><?php the_title(); ?></span>
											<?php } ?>
											<!-- <span class="news-title"><?php echo substr(get_the_title(), 0, 64) . "…"; ?></span>  -->
										</a>
										<!-- <p><?php echo substr(get_the_excerpt(), 0, 120) . "…"; ?></p> -->
										<p><?php echo get_the_excerpt(); ?></p>
										<a href="<?php the_permalink(); ?>" class="read-more"><?php _e("Read More", "balfin")  ?></a>
									</div>
								</div>
							</div>

					<?php endwhile;
					endif;
					wp_reset_postdata(); ?>
				</div>
			</div>
		</section>


		<section class="section-news sus board-members-section">
			<div class="grid-container">
				<div class="grid-x grid-padding-x">
					<div class="cell medium-12">
						<div class="default-title center has-decor wow fadeInUp"><?php _e("CSR Reports", "balfin")  ?></div>
					</div>
					<!-- <?php
							$delay = 0;
							if (have_rows('sustainability_files')) : while (have_rows('sustainability_files')) : the_row(); ?>
			<div class="cell medium-3 text-center">
				<a class="image-cover wow fadeInUp" data-wow-delay="<?php echo $delay; ?>s" target="blank" href="<?php the_sub_field('file') ?>">
					<img src="<?php the_sub_field('cover') ?>" alt="">
				</a>
				<a class="download-button wow fadeInUp" data-wow-delay="<?php echo $delay; ?>s" target="blank" href="<?php the_sub_field('file') ?>" download>
					<span class="icon"><img src="<?php bloginfo('template_url') ?>/img/download.svg" alt=""></span>
					<span class="text"><?php the_sub_field('title') ?></span>
				</a>
			</div>
			<?php $delay = $delay + 0.2;
									if ($delay > 0.9) {
										$delay = 0;
									}
								endwhile;
							endif; ?> -->
					<div class="new-numbers-wrapper fadeInRight wow" style="width: 100%;">

						<?php
						$args1 = array(
							'post_type' => 'file',
							'posts_per_page' => -1,
							'file-category' => 'csr-activities'
						);
						$loop1 = new WP_Query($args1);
						if ($loop1->have_posts()) { ?>
							<div class="board-content active" data-content="1">
								<div class="grid-x grid-padding-x">
									<div class="cell medium-12">
										<div class="pub-files">
											<?php if ($loop1->have_posts()) : while ($loop1->have_posts()) : $loop1->the_post(); ?>
													<div class="single-file">
														<div class="title">
															<a href="<?php the_field('upload_file') ?>" title="Download File" target="_blank">
																<?php the_title(); ?>
															</a>
														</div>
														<div class="download">
															<a href="<?php the_field('upload_file') ?>" title="Download File" download>
																<img src="<?php bloginfo('template_url') ?>/img/download.svg" alt="">
															</a>
														</div>
													</div>
											<?php endwhile;
											endif;
											wp_reset_postdata(); ?>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>


		<!-- <div class="section-text">
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-4">
						<div class="text-holder">
							<div class="text-block">
								<div class="default-title has-decor"><?php _e("Adhere to UN SDG", "balfin")  ?></div>
								<p><?php _e("We support the United Nations’ 17 Sustainable Development Goals. These Global Goals are closely linked to our core sustainability areas of ", "balfin")  ?><strong><?php _e("Health and Safety, Ethics, Green, Community Investment and Diversity and Inclusion.", "balfin")  ?></strong></p>
							</div>
						</div>
					</div>
					<div class="cell medium-1"></div>
					<div class="cell medium-7">
						<div class="image-wrapper">
							<img src="<?php bloginfo('template_url') ?>/img/sdglogocolor.svg" alt="" style="height: auto;max-height: 400px;width: 100%;">
							
						</div>
					</div>
					<div class="cell medium-12">
						<div class="default-title" style="margin-top: 70px;font-size:42px;margin-bottom:30px;"><?php _e("Delivering on purpose", "balfin")  ?></div>
						<p>
							<?php _e("Sustainability is grounded in our values and way of working. This enables us to deliver better solutions for customers through our companies, drive operational efficiency, attract employees, manage risk and support society in many ways. In this perspective, sustainability is also our business advantage, contributing to deliver profit and shareholder value.<br><br>
							We have an ambition and ability to contribute to the United Nations Sustainable Development Goals. Our business nature guides us on which goals to focus on. We have identified 6 goals that are of particular importance.<br><br>
							Learn how these goals link to our sustainability focus areas and which activities we are actively engaged in.", "balfin")  ?>
						</p>
					</div>
					<div class="bottom-icons">
						<div class="grid-x align-center">
							<div class="cell medium-2">
								<div class="single-sus-icon bottom">
									<div class="icon">
										<img src="<?php bloginfo('template_url') ?>/img/icons/goal1.jpg" alt="goal1">
									</div>
									<div class="title"><?php _e("End poverty in all its forms everywhere", "balfin")  ?></div>
								</div>
							</div>
							<div class="cell medium-2">
								<div class="single-sus-icon bottom">
									<div class="icon">
										<img src="<?php bloginfo('template_url') ?>/img/icons/goal2.jpg" alt="goal2">
									</div>
									<div class="title"><?php _e("End hunger, achieve food security", "balfin")  ?></div>
								</div>
							</div>
							<div class="cell medium-2">
								<div class="single-sus-icon bottom">
									<div class="icon">
										<img src="<?php bloginfo('template_url') ?>/img/icons/goal3.jpg" alt="goal3">
									</div>
									<div class="title"><?php _e("Ensure healthy lives and promote well-being for all at all ages", "balfin")  ?></div>
								</div>
							</div>
							<div class="cell medium-2">
								<div class="single-sus-icon bottom">
									<div class="icon">
										<img src="<?php bloginfo('template_url') ?>/img/icons/goal4.jpg" alt="goal4">
									</div>
									<div class="title"><?php _e("Ensure inclusive and equitable quality education and promote lifelong learning opportunities for all", "balfin")  ?></div>
								</div>
							</div>
							<div class="cell medium-2">
								<div class="single-sus-icon bottom">
									<div class="icon">
										<img src="<?php bloginfo('template_url') ?>/img/icons/goal8.jpg" alt="goal8">
									</div>
									<div class="title"><?php _e("Promote sustained, inclusive and sustainable economic growth, full and productive employment and decent work for all", "balfin")  ?></div>
								</div>
							</div>
							<div class="cell medium-2">
								<div class="single-sus-icon bottom">
									<div class="icon">
										<img src="<?php bloginfo('template_url') ?>/img/icons/goal17.jpg" alt="goal17">
									</div>
									<div class="title"><?php _e("Strengthen the means of implementation and revitalize the global partnership for sustainable development", "balfin")  ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> -->




<?php endwhile;
endif; ?>
<?php get_footer(); ?>