<?php /* Template Name: Governance */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


		<section>
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="breadcrumbs">
							<p id="breadcrumbs">
								<span><span><a href="https://balfin.al/"><?php _e("Home", "balfin")  ?></a> &gt; <span><span><?php _e("About Us", "balfin")  ?></span> &gt; <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>



		<div class="grid-container">
			<div class="grid-x">
				<div class="cell medium-12">
					<h1 class="big-title">
						<?php the_title(); ?>
					</h1>
				</div>
			</div>
		</div>

		<div class="section-text no-pad-top">
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-5">
						<div class="text-holder">
							<div class="text-block">
								<div class="default-title has-decor"><?php _e("Corporate Governance", "balfin")  ?></div>
								<?php the_content(); ?>
								<div style="margin-top: 80px;">
									<strong>Download BALFIN Group’s Corporate Governance Directive:</strong>
									<a href="<?php bloginfo('template_url') ?>/img/BALFIN_Group_Corporate_Governance_18.07.2022.pdf" download target="_blank" alt="Download" class="with-icon"><img src="<?php bloginfo('template_url') ?>/img/download.svg" alt=""></a>
								</div>
							</div>
						</div>
					</div>
					<div class="cell medium-1"></div>
					<div class="cell medium-6">
						<div class="image-wrapper ">
							<img src="<?php bloginfo('template_url'); ?>/img/corporategov.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- <div class="section-text">
	<div class="grid-container">
		<div class="grid-x">   
			<div class="cell medium-12">
				<div class="text-holder">
					<div class="text-block">
						<div class="default-title  has-decor">Corporate Governance Framework</div>
						<p>Effective corporate governance requires a clear understanding of the respective roles that constitute the management of the group and their relationships with others in the corporate structure.<br>This is performed in line with the overall objectives, plans and goals of business. Corporate Governance is integrated into a business strategy and objectives and is not viewed simply as a compliance obligation, separate from the long-term business prospects, plans and objectives.&nbsp;<br></p>
						<div class="hidden-content" data-content="1">
							<p><br>THE MAIN GOAL OF OUR CORPORATE GOVERNANCE IS TO RELY ON:</p>
							<ul><li>Business Ethics</li><li>Strategic Management</li><li>Aligned Business Goals</li><li>Effective Organization</li><li>Disclosure, Transparency &amp; Accountability</li><li>Innovation and Continues Improvement</li><li>Compliance with Laws and Regulations.</li></ul>



							<p>This is performed in line with the overall objectives, plans and goals of business. Corporate Governance is integrated into a business` strategy and objectives and is not viewed simply as a compliance obligation, separate from the business` long-term business prospects, plans and objectives.</p>



							<p>The Governing body of Balfin Group is the Management Board, which is composed of:</p>



							<ul><li>CEO of Balfin Group</li><li>4 Vice Presidents</li><li>CFO of Balfin Group</li><li>Partner for Kosovo &amp; North Macedonia</li></ul>



							<p>In our Corporate Governance we are guided by the principles that conserve the best aspects of an entrepreneurial culture which has brought success in the past while introducing the best aspects of professional management. Balfin Group’s competitive advantage comes from developing its human capital. We have established talent attraction and nomination procedures for top management positions in the Group and the subsidiary levels and have increased the awareness and knowledge of managers on good governance benefits, thus involving them in governance improvement plan.</p>
						</div>
						<div class="read-more" data-open="1"><span class="readmore">Show More</span><span class="readless">Show Less</span></div>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
</div> -->

		<div class="section-text">
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="text-holder">
							<div class="text-block">
								<div class="default-title  has-decor"><?php _e("Governance Bodies", "balfin")  ?> </div>
								<p><?php _e("BALFIN Group has the following decision-making bodies", "balfin")  ?>: </p>
								<ul>
									<li><?php _e("President of BALFIN Group", "balfin")  ?>;</li>
									<li><?php _e("CEO of BALFIN Group", "balfin")  ?></li>
									<li><?php _e("Group Management Board", "balfin")  ?>;</li>
									<li><?php _e("Vice Presidents", "balfin")  ?>;</li>
									<li><?php _e("CEO of Group Companies", "balfin")  ?>.</li>
								</ul>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>


		<div class="board-members-section">
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="board-tabs">
							<div class="single-tab active" data-tab="1"><span><?php _e("Management Board", "balfin")  ?></span></div>
							<div class="single-tab" data-tab="2"><span><?php _e("Functional Directors", "balfin")  ?></span></div>
						</div>
						<div class="board-content active" data-content="1">
							<div class="grid-x grid-padding-x">
								<?php if (have_rows('board_members')) : while (have_rows('board_members')) : the_row(); ?>

										<?php
										$number = get_sub_field('number');
										$name = get_sub_field('name');
										$slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $name)));
										if ($number == 1) {
										?>
											<div class="cell medium-3">
												<div class="single-member-wrapper" data-member="<?php echo $slug; ?>">
													<img src="<?php the_sub_field('photo'); ?>" alt="">
													<div class="overlay-content">
														<div class="title"><?php the_sub_field('name'); ?></div>
														<div class="role"><?php the_sub_field('role'); ?></div>
													</div>
												</div>
											</div>
								<?php }
									endwhile;
								endif; ?>
							</div>
						</div>
						<div class="board-content" data-content="2">
							<div class="grid-x grid-padding-x">
								<?php if (have_rows('board_members')) : while (have_rows('board_members')) : the_row(); ?>

										<?php
										$number = get_sub_field('number');
										$name = get_sub_field('name');
										$slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $name)));
										if ($number == 2) {
										?>
											<div class="cell medium-3">
												<div class="single-member-wrapper" data-member="<?php echo $slug; ?>">
													<img src="<?php the_sub_field('photo'); ?>" alt="">
													<div class="overlay-content">
														<div class="title"><?php the_sub_field('name'); ?></div>
														<div class="role"><?php the_sub_field('role'); ?></div>
													</div>
												</div>
											</div>
								<?php }
									endwhile;
								endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="section-text departaments gray">
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="text-holder">
							<div class="text-block">
								<div class="default-title has-decor"><?php _e("DEPARTAMENTS", "balfin")  ?></div>
							</div>
						</div>
					</div>
					<div class="cell medium-12">
						<div class="grid-x grid-padding-x">
							<div class="cell medium-6">
								<?php
								$counter = 1;
								if (have_rows('departaments')) : while (have_rows('departaments')) : the_row();
										if ($counter < 5) {
								?>
											<div class="single-accordion">
												<div class="accordion-title" data-open="<?php echo ($counter); ?>">
													<div class="icon" style="background-image: url(<?php the_sub_field('icon') ?>);"></div>
													<div class="title"><?php the_sub_field('title'); ?></div>
												</div>
												<div class="accordion-content" data-content="<?php echo ($counter); ?>">
													<?php the_sub_field('content'); ?>
												</div>
											</div>
								<?php  }
										$counter++;
									endwhile;
								endif; ?>
							</div>
							<div class="cell medium-6">
								<?php
								$counter = 1;
								if (have_rows('departaments')) : while (have_rows('departaments')) : the_row();
										if ($counter > 4) {
								?>
											<div class="single-accordion">
												<div class="accordion-title" data-open="<?php echo ($counter); ?>">
													<div class="icon" style="background-image: url(<?php the_sub_field('icon') ?>);"></div>
													<div class="title"><?php the_sub_field('title'); ?></div>
												</div>
												<div class="accordion-content" data-content="<?php echo ($counter); ?>">
													<?php the_sub_field('content'); ?>
												</div>
											</div>
								<?php  }
										$counter++;
									endwhile;
								endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>



		<div class="section-text  code">
			<div class="grid-container">
				<div class="grid-x grid-padding-x">
					<div class="cell medium-4">
						<div class="image-wrapper">
							<img src="<?php bloginfo('template_url'); ?>/img/corporategov2.jpg" alt="corporate gov">
						</div>
					</div>
					<div class="cell medium-8">
						<div class="text-holder">
							<div class="text-block">


								<?php if (have_rows('code_of_ethics')) : ?>
									<?php while (have_rows('code_of_ethics')) : the_row();  ?>



										<div class="default-title"><?php the_sub_field('title'); ?></div>
										<!-- <div class="divider-vertical"></div> -->
										<p class="small"><?php the_sub_field('paragraph1'); ?><br>
											<?php the_sub_field('paragraph2'); ?><br><br>

											<strong><?php the_sub_field('download_text'); ?></strong>

											<a href="<?php bloginfo('template_url') ?>/img/balfin-group-code-of-conduct-2023.pdf" download target="_blank" alt="Download" class="with-icon"><img src="<?php bloginfo('template_url') ?>/img/download.svg" alt=""></a>
										</p>

									<?php endwhile; ?>
								<?php endif; ?>



							</div>
						</div>
					</div>

				</div>
			</div>
		</div>


		<?php if (have_rows('board_members')) : while (have_rows('board_members')) : the_row(); ?>
				<?php
				$name = get_sub_field('name');
				$slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $name)));
				?>
				<div class="member-popup" data-popup="<?php echo $slug; ?>">
					<div class="overlay"></div>
					<div class="popup-inner">
						<div class="close-popup">&times;</div>
						<div class="grid-x grid-padding-x">
							<div class="cell medium-3">
								<img src="<?php the_sub_field('photo'); ?>" alt="">
							</div>
							<div class="cell medium-9">
								<div class="title"><?php the_sub_field('name'); ?></div>
								<div class="role"><?php the_sub_field('role'); ?></div>
								<div class="content">
									<?php the_sub_field('short_content'); ?>
									<?php the_sub_field('long_content'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
		<?php endwhile;
		endif; ?>



<?php endwhile;
endif; ?>
<?php get_footer(); ?>