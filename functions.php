<?php

add_theme_support('menus');
add_theme_support('widgets');
add_theme_support('post-thumbnails');


function tok_scripts()
{

    wp_enqueue_style('font-awesome', 'https://use.fontawesome.com/releases/v5.7.2/css/all.css', '', '', false);
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Cinzel:400,700,900&display=swap', '', '', false);
    wp_enqueue_style('wow-stylesheet', get_stylesheet_directory_uri() . '/scss/animate.css?v' . time(), '', '', false);
    wp_enqueue_style('main-stylesheet', get_stylesheet_directory_uri() . '/css/app.css?v' . time(), '', '', false);
    wp_enqueue_style('lightbox2-stylesheet', get_stylesheet_directory_uri() . '/node_modules/lightbox2/dist/css/lightbox.min.css', '', '', false);
    wp_enqueue_style('slick-stylesheet', get_stylesheet_directory_uri() . '/node_modules/slick-carousel/slick/slick.css', '', '', false);

    wp_deregister_script('jquery');

    wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/node_modules/jquery/dist/jquery.min.js', array(), '', false);

    wp_enqueue_script('what-input', get_stylesheet_directory_uri() . '/node_modules/what-input/dist/what-input.min.js', array(), '', true);
    wp_enqueue_script('foundation', get_stylesheet_directory_uri() . '/node_modules/foundation-sites/dist/js/foundation.min.js', array('jquery'), '', true);
    wp_enqueue_script('slick-carousel', get_stylesheet_directory_uri() . '/node_modules/slick-carousel/slick/slick.min.js', array('jquery'), '', true);
    wp_enqueue_script('lightbox2', get_stylesheet_directory_uri() . '/node_modules/lightbox2/dist/js/lightbox.min.js', array('jquery'), '', true);
    wp_enqueue_script('circles-scripts', get_stylesheet_directory_uri() . '/js/circles.min.js?v' . time(), array(), '', true);
    wp_enqueue_script('wow-scripts', get_stylesheet_directory_uri() . '/js/wow.min.js?v' . time(), array(), '', true);
    wp_enqueue_script('tok-scripts', get_stylesheet_directory_uri() . '/js/app.js?v' . time(), array(), '', true);
}


add_action('wp_enqueue_scripts', 'tok_scripts');

function custom_posttype()
{
    register_post_type(
        'file',
        array(
            'labels' => array(
                'name' => __('Files'),
                'singular_name' => __('file')
            ),
            'public' => false,
            'has_archive' => true,
            'supports' => array('thumbnail', 'title', 'editor', 'excerpt'),
            'taxonomies'  => array('file-category')
        )
    );
    register_post_type(
        'gallery',
        array(
            'labels' => array(
                'name' => __('Galleries'),
                'singular_name' => __('gallery')
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('thumbnail', 'title', 'editor', 'excerpt'),
            'taxonomies'  => array('gallery-category')
        )
    );
    register_post_type(
        'job',
        array(
            'labels' => array(
                'name' => __('jobs'),
                'singular_name' => __('job')
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('thumbnail', 'title', 'editor', 'excerpt'),
            'taxonomies'  => array('job-category', 'job-location', 'job-company', 'job-industry')
        )
    );
}
add_action('init', 'custom_posttype');

function custom_taxonomies()
{

    register_taxonomy(
        'file-category',
        'file',
        array(
            'label' => __('Kategorite'),
            'rewrite' => array('slug' => 'file-category'),
            'hierarchical' => true,
        )
    );
    register_taxonomy(
        'gallery-category',
        'gallery',
        array(
            'label' => __('Kategorite'),
            'rewrite' => array('slug' => 'gallery-category'),
            'hierarchical' => true,
        )
    );
    register_taxonomy(
        'job-location',
        'job',
        array(
            'label' => __('Vendndodhja'),
            'rewrite' => array('slug' => 'job-location'),
            'hierarchical' => true,
        )
    );
    register_taxonomy(
        'job-company',
        'job',
        array(
            'label' => __('Kompania'),
            'rewrite' => array('slug' => 'job-company'),
            'hierarchical' => true,
        )
    );
}
add_action('init', 'custom_taxonomies');
