<div class="cell medium-12">
	<div class="organigrama">
		<div class="graph-wrapper">
			<div class="group">
				<div class="single-member-box" data-member="1">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/samir_mane.png" alt="">
					</div>
					<div class="name">Samir Mane</div>
					<div class="role">President</div>
				</div>
			</div>

			<div class="group multiple second-row">
				<!-- <div class="single-member-box line-top long" data-member="7">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/ervin_kajno.png" alt="">
					</div>
					<div class="name">Ervin Kajno</div>
					<div class="role">Director Corporate Projects Department</div>
				</div> -->
				<div class="single-member-box line-top line-top-short line-bottom" data-member="3">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/edlira_muka.png" alt="">
					</div>
					<div class="name">Edlira Muka</div>
					<div class="role"><?php _e("Chief Executive Officer" , "balfin")  ?></div>
				</div>
				<!-- <div class="single-member-box line-top long" data-member="4">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/shpetim_spahija.png" alt="">
					</div>
					<div class="name">Shpetim Spahija </div>
					<div class="role">Office of the President</div>
				</div> -->
			</div>

			<div class="group has-line-center"></div>

			<div class="group multiple gray">
				<div class="single-member-box line-top" data-member="7">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/ervin_kajno.png" alt="">
					</div>
					<div class="name">Ervin Kajno</div>
					<div class="role"><?php _e("Vice President" , "balfin")  ?><br>
						<?php _e("Director of Coroporate Projects Department" , "balfin")  ?>
					</div>
				</div>
				<div class="single-member-box line-top" data-member="6">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/julian_mane.png" alt="">
					</div>
					<div class="name">Julian Mane</div>
					<div class="role"><?php _e("Vice President" , "balfin")  ?><br>
						<?php _e("Retail & Real Estate Management" , "balfin")  ?></div>
				</div>
				<div class="single-member-box line-top" data-member="5">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/steven.png" alt="">
					</div>
					<div class="name">Steven Grunerud</div>
					<div class="role"><?php _e("Vice President" , "balfin")  ?><br>
						<?php _e("Mergers & Aquisitions" , "balfin")  ?></div>
				</div>
				<div class="single-member-box line-top" data-member="8">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/adriana_sokoli.png" alt="">
					</div>
					<div class="name">Adriana Sokoli</div>
					<div class="role"><?php _e("Vice President" , "balfin")  ?><br>
						<?php _e("Real Estate" , "balfin")  ?></div>
				</div>
				<div class="single-member-box line-top" data-member="9">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/arsim_papraniku.png" alt="">
					</div>
					<div class="name">Arsim Parpraniku</div>
					<div class="role"><?php _e("Partner" , "balfin")  ?><br>
						<?php _e("Macedonia & Kosovo" , "balfin")  ?></div>
				</div>
				<div class="single-member-box line-top" data-member="10">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/blerina_daka.png" alt="">
					</div>
					<div class="name">Blerina Daka</div>
					<div class="role"><?php _e("Chef Financial Officer" , "balfin")  ?></div>
				</div>
			</div>

			<!-- <div class="group multiple directors">
				<div class="single-director">
					Director Of Company
				</div>
				<div class="single-director">
					Director Of Company
				</div>
				<div class="single-director">
					Director Of Company
				</div>
				<div class="single-director">
					Director Of Company
				</div>
				<div class="empty"></div>
				<div class="empty"></div>
			</div> -->

			<div class="group has-line-center"></div>

			<div class="group multiple">
				<div class="single-member-box line-top" data-member="10">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/blerina_daka.png" alt="">
					</div>
					<div class="name">Blerina Daka</div>
					<div class="role"><?php _e("Chef Financial Officer" , "balfin")  ?><br>
						<?php _e("Finance Department" , "balfin")  ?></div>
				</div>
				<div class="single-member-box line-top" data-member="11">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/agim_fjolla.png" alt="">
					</div>
					<div class="name">Agim Fjolla</div>
					<div class="role"><?php _e("Director Communications Department" , "balfin")  ?></div>
				</div>
				<div class="single-member-box line-top" data-member="12">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/gjergji_spaho.png" alt="">
					</div>
					<div class="name">Gjergji Spaho</div>
					<div class="role"><?php _e("Chief Informations Officer" , "balfin")?><br>
						<?php _e("Informations System Division" , "balfin")  ?></div>
				</div>
				<div class="single-member-box line-top" data-member="13">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/itena_ndroqi.png" alt="">
					</div>
					<div class="name">Itena Ndroqi</div>
					<div class="role"><?php _e("Director" , "balfin")?><br>
						<?php _e("Legal Department" , "balfin")  ?></div>
				</div>
				<div class="single-member-box line-top" data-member="14">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/elvis_nosi.png" alt="">
					</div>
					<div class="name">Elvin Nosi</div>
					<div class="role"><?php _e("Director" , "balfin")?><br>
						<?php _e("HR Department" , "balfin")  ?></div>
				</div>
				<div class="single-member-box line-top" data-member="15">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/ardi_hafizi.png" alt="">
					</div>
					<div class="name">Ardi Hafizi</div>
					<div class="role"><?php _e("Director" , "balfin")?><br>
						<?php _e("Internal Audit Department" , "balfin")  ?></div>
				</div>
				<div class="single-member-box line-top" data-member="16">
					<div class="img-holder">
						<img src="<?php bloginfo('template_url') ?>/img/organigrama/matilda_shehu.png" alt="">
					</div>
					<div class="name">Matilda Shehu</div>
					<div class="role"><?php _e("Director of Business Operations" , "balfin")  ?></div>
				</div>
			</div>
		</div>
	</div>
</div>

