<?php /* Template Name: Publications */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


		<section>
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="breadcrumbs">
							<p id="breadcrumbs"><span><span><a href="https://balfin.al/"><?php _e("Home", "balfin")  ?></a> &gt; <span><?php _e("Media Center", "balfin")  ?> &gt; <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></span></p>
						</div>
					</div>
				</div>
			</div>
		</section>


		<?php
		$args0 = array(
			'post_type' => 'file',
			'posts_per_page' => -1
		);
		$args1 = array(
			'post_type' => 'file',
			'posts_per_page' => -1,
			'file-category' => 'annual-report'
		);
		$args2 = array(
			'post_type' => 'file',
			'posts_per_page' => -1,
			'file-category' => 'company-presentation'
		);
		$args3 = array(
			'post_type' => 'file',
			'posts_per_page' => -1,
			'file-category' => 'balfin-times'
		);
		$args4 = array(
			'post_type' => 'file',
			'posts_per_page' => -1,
			'file-category' => 'csr-activities'
		);
		$loop2 = new WP_Query($args2);
		$loop3 = new WP_Query($args3);
		$loop4 = new WP_Query($args4);
		$loop1 = new WP_Query($args1);
		$loop0 = new WP_Query($args0);
		?>
		<div class="board-members-section">
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="default-title has-decor center"><?php the_title(); ?></div>
					</div>
				</div>
			</div>
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell medium-12">
						<div class="board-tabs">
							<?php if ($loop0->have_posts()) { ?><div class="single-tab active" data-tab="0"><span><?php _e("All Publications", "balfin")  ?></span></div> <?php } ?>
							<?php if ($loop1->have_posts()) { ?><div class="single-tab" data-tab="1"><span><?php _e("Annual Report", "balfin")  ?></span></div> <?php } ?>
							<?php if ($loop2->have_posts()) { ?><div class="single-tab" data-tab="2"><span><?php _e("Company Presentation", "balfin")  ?></span></div><?php } ?>
							<?php if ($loop3->have_posts()) { ?><div class="single-tab" data-tab="3"><span><?php _e("Balfin Times", "balfin")  ?></span></div><?php } ?>
							<?php if ($loop4->have_posts()) { ?><div class="single-tab" data-tab="4"><span><?php _e("CSR Activities", "balfin")  ?></span></div><?php } ?>
						</div>
						<?php if ($loop0->have_posts()) { ?>
							<div class="board-content active" data-content="0">
								<div class="grid-x grid-padding-x">
									<div class="pub-files">
										<?php if ($loop0->have_posts()) : while ($loop0->have_posts()) : $loop0->the_post(); ?>
												<div class="single-file">
													<div class="title">
														<a href="<?php the_field('upload_file') ?>" title="Download File" target="_blank">
															<?php the_title(); ?>
														</a>
													</div>
													<div class="download">
														<a href="<?php the_field('upload_file') ?>" title="Download File" download>
															<img src="<?php bloginfo('template_url') ?>/img/download.svg" alt="">
														</a>
													</div>
												</div>
										<?php endwhile;
										endif;
										wp_reset_postdata(); ?>
									</div>
								</div>
							</div>
						<?php } ?>
						<?php if ($loop1->have_posts()) { ?>
							<div class="board-content" data-content="1">
								<div class="grid-x grid-padding-x">
									<div class="pub-files">
										<?php if ($loop1->have_posts()) : while ($loop1->have_posts()) : $loop1->the_post(); ?>
												<div class="single-file">
													<div class="title">
														<a href="<?php the_field('upload_file') ?>" title="Download File" target="_blank">
															<?php the_title(); ?>
														</a>
													</div>
													<div class="download">
														<a href="<?php the_field('upload_file') ?>" title="Download File" download>
															<img src="<?php bloginfo('template_url') ?>/img/download.svg" alt="">
														</a>
													</div>
												</div>
										<?php endwhile;
										endif;
										wp_reset_postdata(); ?>
									</div>
								</div>
							</div>
						<?php } ?>
						<?php if ($loop2->have_posts()) { ?>
							<div class="board-content" data-content="2">
								<div class="grid-x grid-padding-x">
									<div class="pub-files">
										<?php if ($loop2->have_posts()) : while ($loop2->have_posts()) : $loop2->the_post(); ?>
												<div class="single-file">
													<div class="title">
														<a href="<?php the_field('upload_file') ?>" title="Download File" target="_blank">
															<?php the_title(); ?>
														</a>
													</div>
													<div class="download">
														<a href="<?php the_field('upload_file') ?>" title="Download File" download>
															<img src="<?php bloginfo('template_url') ?>/img/download.svg" alt="">
														</a>
													</div>
												</div>
										<?php endwhile;
										endif;
										wp_reset_postdata(); ?>
									</div>
								</div>
							</div>
						<?php } ?>
						<?php if ($loop3->have_posts()) { ?>
							<div class="board-content" data-content="3">
								<div class="grid-x grid-padding-x">
									<div class="pub-files">
										<?php if ($loop3->have_posts()) : while ($loop3->have_posts()) : $loop3->the_post(); ?>
												<div class="single-file">
													<div class="title">
														<a href="<?php the_field('upload_file') ?>" title="Download File" target="_blank">
															<?php the_title(); ?>
														</a>
													</div>
													<div class="download">
														<a href="<?php the_field('upload_file') ?>" title="Download File" download>
															<img src="<?php bloginfo('template_url') ?>/img/download.svg" alt="">
														</a>
													</div>
												</div>
										<?php endwhile;
										endif;
										wp_reset_postdata(); ?>
									</div>
								</div>
							</div>
						<?php } ?>
						<?php if ($loop4->have_posts()) { ?>
							<div class="board-content" data-content="4">
								<div class="grid-x grid-padding-x">
									<div class="pub-files">
										<?php if ($loop4->have_posts()) : while ($loop4->have_posts()) : $loop4->the_post(); ?>
												<div class="single-file">
													<div class="title">
														<a href="<?php the_field('upload_file') ?>" title="Download File" target="_blank">
															<?php the_title(); ?>
														</a>
													</div>
													<div class="download">
														<a href="<?php the_field('upload_file') ?>" title="Download File" download>
															<img src="<?php bloginfo('template_url') ?>/img/download.svg" alt="">
														</a>
													</div>
												</div>
										<?php endwhile;
										endif;
										wp_reset_postdata(); ?>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>


		<!-- <div class="csr-popup">
	<div class="inner">
		<div class="close-button">
			<img src="<?php bloginfo('template_url') ?>/img/close.svg" alt="">
		</div>
		<a href="https://balfin.al/wp-content/uploads/2020/11/Annual-Report-2018-Balfin_final-INTERACTIVE_72-min.pdf" target="_blank">
			<img src="https://balfin.al/wp-content/uploads/2020/12/annual_report_2018.png" alt="">
		</a>
	</div>
</div> -->


<?php endwhile;
endif; ?>
<?php get_footer(); ?>