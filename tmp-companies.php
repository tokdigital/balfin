<?php /* Template Name: Group Companies */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs">
					<span><span><a href="https://balfin.al/"><?php _e("Home" , "balfin")  ?></a> &gt; <span><span><?php _e("About Us" , "balfin")  ?></span> &gt; <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></span>
					</p>
				</div>
			</div>
		</div>
	</div>	
</section>


<div class="grid-container">
	<div class="grid-x">
		<div class="cell medium-12">
			<div class="big-title">
				<?php the_title(); ?>
			</div>
		</div>
		<div class="cell medium-12">
			<p><?php the_content(); ?></p>
		</div>
	</div>
</div>


<div class="section-logos">
	<div class="grid-container">
		<div class="grid-x">
			<?php 
			if (have_rows('companies')) { 
				while (have_rows('companies')) { 
					the_row();
			?>
				<?php if (get_sub_field('sub_companies')){ ?>
				<div class="cell medium-3">
					<div class="logo-wrapper ">
						<div class="nolink">
							<img src="<?php the_sub_field('logo'); ?>" alt="">
						</div>
						<div class="other-companies">
							<?php 
								if (have_rows('sub_companies')) { 
									while (have_rows('sub_companies')) { 
										the_row();
								?>
								<a href="<?php the_sub_field('link2'); ?>" target="_blank">
									<img src="<?php the_sub_field('logo2'); ?>" alt="">
									<p><?php the_sub_field('state') ?></p>
								</a>
							<?php }} ?>
						</div>
					</div>
				</div>
				<?php } else { ?>
				<div class="cell medium-3">
					<div class="logo-wrapper">
						<a href="<?php the_sub_field('link'); ?>" target="_blank">
							<img src="<?php the_sub_field('logo'); ?>" alt="">
						</a>
					</div>
				</div>
			<?php 
				}
				} 
			}
			?>
		</div>
	</div>
</div> 


	
<?php endwhile;endif; ?>
<?php get_footer(); ?>