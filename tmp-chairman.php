<?php /* Template Name: Chairman */ ?>
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>


<section>
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="breadcrumbs">
					<p id="breadcrumbs">
					<span><span><a href="https://balfin.al/"></a> &gt; <span><span><?php _e("About Us" , "balfin")  ?></span> &gt; <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span></span></span></span>
					</p>
				</div>
			</div>
		</div>
	</div>	
</section>

<section class="chairman">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-5">
				<div class="img-wrapper2">
					<img src="<?php bloginfo('template_url') ?>/img/chairman4.jpg" alt="chairman" id="chairman-image2" style="margin-bottom: 20px;"> 
				</div>
			</div>
			<div class="cell medium-7">
				<div class="content">
					<h1 class="default-title">
					<?php _e("President's Note" , "balfin")  ?>
					</h1>
					<?php the_content(); ?>
				</div>
				<p><strong>Samir Mane</strong><br><?php _e("President of BALFIN Group" , "balfin")  ?></p>
			</div>
		</div>
	</div>
</section>


	

<?php endwhile;endif; ?>
<?php get_footer(); ?>